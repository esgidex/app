Document de conception du projet :

Auteurs : Lattana, Yanis et Julien

## Introduction

Réalisation d'une application web de type DEX (Decentralized Exchange) sur la blockchain Ethereum.

Les technologies utilisées sont les suivantes :

- NextJS
- TailwindCSS
- Docker
- Sonarqube
- Gitlab Pipeline 
- Typescript


Pourquoi ces technologies ? 

- Utilisation de Nextjs, Pourquoi ? : Nous avons choisi de prendre Nextjs simplement pour son système de routing entre les pages.
- Utilisation de TailwindCSS, Pourquoi ? : Nous avons choisi de prendre TailwindCSS pour sa facilité d'utilisation et sa rapidité de mise en place, de plus tailwinds propose toute une panoplie de petits composants qui nous aiderons à développer notre application.
- Utilisation de Docker, Pourquoi ? : Nous avons choisi de prendre Docker pour sa facilité de mise en place et de déploiement, de plus il nous permettra de mettre en place un environnement de développement commun à tous les membres du groupe.
- Utilisation de Sonarqube, Pourquoi ? : Nous avons choisi de prendre Sonarqube pour son expertise de code coverage, cet outil nous permettra de vérifier la fiabilité de notre code avant chaque push en production.
- Utilisation de Gitlab Pipeline, Pourquoi ? : Nous avons choisi de prendre Gitlab Pipeline afin de permettre l'automatisation de tout notre process.
- Utilisation de Typescript, Pourquoi ? : Nous avons choisi de prendre Typescript pour avoir une structure plus claire de nos données dans le code afin de faciliter leur manipulation.

Page de l'application : 

- SWAP (Permettra de d'échanger un token contre un autre et/ou d'en faire l'achat en monnaie directement)
- STAKING (Permettra de staker un token et de gagner des intérêts)
- LIQUIDITY (Permettra de fournir de la liquidité à la plateforme et de gagner des intérêts)
- Token (Permettra de voir les tokens disponibles sur la plateforme)

Questions : 

Est ce que l'integration de l'abi en db est qql chose de viable ?