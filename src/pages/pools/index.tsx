import React, { useState, useEffect } from "react";
import PoolTable from "@/components/Pool/PoolTable";
import AddLiquidityForm from "@/components/Pool/AddLiquidityForm";
import useCreatePool from "@/hooks/useCreatePool";
import { useAdminStatus } from "@/hooks/useAdminStatus";
import useDeletePool from "@/hooks/useDeletePool";
import { toast } from "react-toastify";
import useGetPoolDetails from "@/hooks/useGetPoolDetails";
import Layout from "@/components/Layout/Layout";
import { useGetAllPools } from "@/hooks/useGetAllPools";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";

function PoolPage() {
  const [selectedPool, setSelectedPool] = useState<`0x${string}` | null>(null);
  const { allPools: pools } = useGetAllPools();
  const [poolList, setPoolList] = useState<`0x${string}`[]>([]);
  const [deletingPools, setDeletingPools] = useState<`0x${string}`[]>([]);

  useEffect(() => {
    if (pools && Array.isArray(pools)) {
      setPoolList(pools as `0x${string}`[]);
    }
  }, [pools]);

  const {
    tokenA,
    setTokenA,
    tokenB,
    setTokenB,
    priceFeedA,
    setPriceFeedA,
    priceFeedB,
    setPriceFeedB,
    handleCreatePool,
    isCreating,
    isSuccess,
    error,
    newPoolAddress,
  } = useCreatePool();
  const isAdmin = useAdminStatus();

  const poolDetails = useGetPoolDetails(selectedPool as `0x${string}`);

  const {
    handleDeletePool,
    isDeleting,
    isSuccess: isDeleteSuccess,
    isError,
    error: deleteError,
  } = useDeletePool();

  const handleSelectPool = (poolAddress: `0x${string}`) => {
    setSelectedPool(poolAddress);
  };

  const handleDelete = async (poolAddress: `0x${string}`) => {
    setDeletingPools((prev) => [...prev, poolAddress]);
    const success = await handleDeletePool(poolAddress);
    if (success) {
      toast.success("Pool supprimée avec succès");
      setPoolList((prevPools) =>
        prevPools.filter((pool) => pool !== poolAddress)
      );
    } else if (isError) {
      toast.error(`Erreur lors de la suppression de la pool : ${deleteError}`);
    }
    setDeletingPools((prev) => prev.filter((pool) => pool !== poolAddress));
  };

  return (
    <Layout>
      <div className="flex w-full flex-col md:px-8">
        <h1 className="text-3xl">Liquidity Pools</h1>
        <br></br>
        <p>
          Here you can create new pools, add liquidity to existing pools, and
          remove pools.
        </p>
        <br></br>
        <main className="flex flex-1 flex-col gap-4 md:gap-8">
          <PoolTable
            pools={poolList}
            onSelectPool={handleSelectPool}
            onDeletePool={handleDelete}
            isDeleting={isDeleting}
            deletingPools={deletingPools}
          />

          {isAdmin && (
            <div className="grid grid-cols-2 gap-6">
              <div className="flex flex-col">
                <h2 className="text-center text-2xl font-bold text-white mb-4">
                  Create New Pool
                </h2>
                <div className="flex flex-col justify-content items-center">
                  <div className="w-full border p-12 rounded-lg">
                    <form onSubmit={handleCreatePool}>
                      <div className="mb-4">
                        <Label
                          className="block text-white text-sm font-bold mb-2"
                          htmlFor="tokenA"
                        >
                          Token A Address
                        </Label>
                        <Input
                          type="text"
                          id="tokenA"
                          value={tokenA}
                          placeholder="0xcFf6d2C14Fa420aFD90365320AE902381a42F322"
                          onChange={(e) =>
                            setTokenA(e.target.value as `0x${string}`)
                          }
                        />
                      </div>
                      <div className="mb-4">
                        <Label
                          className="block text-white text-sm font-bold mb-2"
                          htmlFor="tokenB"
                        >
                          Token B Address
                        </Label>
                        <Input
                          type="text"
                          id="tokenB"
                          value={tokenB}
                          placeholder="0xcFf6d2C14Fa420aFD90365320AE902381a42F322"
                          onChange={(e) =>
                            setTokenB(e.target.value as `0x${string}`)
                          }
                        />
                      </div>
                      <div className="mb-4">
                        <Label
                          className="block text-white text-sm font-bold mb-2"
                          htmlFor="priceFeedA"
                        >
                          Price Feed A Address
                        </Label>
                        <Input
                          type="text"
                          id="priceFeedA"
                          value={priceFeedA}
                          placeholder="0xcFf6d2C14Fa420aFD90365320AE902381a42F322"
                          onChange={(e) => setPriceFeedA(e.target.value)}
                        />
                      </div>
                      <div className="mb-4">
                        <Label
                          className="block text-white text-sm font-bold mb-2"
                          htmlFor="priceFeedB"
                        >
                          Price Feed B Address
                        </Label>
                        <Input
                          type="text"
                          id="priceFeedB"
                          value={priceFeedB}
                          placeholder="0xcFf6d2C14Fa420aFD90365320AE902381a42F322"
                          onChange={(e) => setPriceFeedB(e.target.value)}
                        />
                      </div>
                      <Button
                        type="submit"
                        disabled={isCreating}
                        className="w-full mt-2"
                      >
                        {isCreating ? "Creating..." : "Create Pool"}
                      </Button>
                    </form>
                    {isCreating && (
                      <div className="mt-2 text-white">
                        Creating pool, please wait...
                      </div>
                    )}
                    {isSuccess && !isCreating && !error && (
                      <div>
                        <p className="mt-2 text-green-500">
                          Pool created successfully!
                        </p>
                      </div>
                    )}
                  </div>
                </div>
              </div>
              {selectedPool && poolDetails && (
                <div className="flex flex-col">
                  <h2 className="text-center text-2xl font-bold text-white mb-4">
                    Add Liquidity
                  </h2>
                  <AddLiquidityForm
                    poolAddress={selectedPool}
                    tokenAAddress={poolDetails.tokenAAddress as `0x${string}`}
                    tokenBAddress={poolDetails.tokenBAddress as `0x${string}`}
                  />
                </div>
              )}
            </div>
          )}
        </main>
      </div>
    </Layout>
  );
}

export default PoolPage;
