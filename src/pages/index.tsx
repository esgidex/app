"use client";

import Layout from "@/components/Layout/Layout";
import SwapForm from "@/components/specifics/SwapForm";

export default function Home() {
  return (
    <Layout>
      <div className="flex w-full flex-col">
        <h1 className="text-3xl md:px-8">Swap your tokens</h1>
        <br></br>
        <p className="md:px-8">
          Exchange your tokens on our decentralized platform.
        </p>
        <br></br>
        <div className="flex justify-center items-center min-h-full p-4 ">
          <SwapForm />
        </div>
      </div>
    </Layout>
  );
};