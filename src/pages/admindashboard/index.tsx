"use client";
import Layout from "@/components/Layout/Layout";
import React, { useState, useEffect } from "react";
import { DollarSign, UserPlus, Trophy, Ban, Users } from "lucide-react";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import UserList from "@/components/AdminDashboard/UserList";
import CardItem from "@/components/AdminDashboard/CardItem";
import MoreStats from "@/components/AdminDashboard/MoreStats";
import useGetAllAdmins from "@/hooks/useGetAllAdmin";
import useGetAllBannedUsers from "@/hooks/useGetAllBannedUsers";
import useAddFunds from "@/hooks/useAddFunds";
import useClaimDexRewards from "@/hooks/useClaimDexRewards";
import { useUsersApi, User } from "@/hooks/useUsersApi";
import { useUser } from "@/context/userContext"; 
import useSetSwapFee from "@/hooks/useSetSwapFee";
import { useGetSwapFees } from "@/hooks/useGetSwapFees";

const Admin: React.FC = () => {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState("");
  const [adminsPage, setAdminsPage] = useState(1);
  const [bannedPage, setBannedPage] = useState(1);
  const [usersPage, setUsersPage] = useState(1);
  const { getUsers } = useUsersApi();
  const [users, setUsers] = useState<User[]>([]);
  const itemsPerPage = 3;
  const { user } = useUser();

  const {
    admins,
    isLoading: isLoadingAdmins,
    error: adminError,
  } = useGetAllAdmins();
  const {
    bannedUsers,
    isLoading: isLoadingBannedUsers,
    error: bannedError,
  } = useGetAllBannedUsers();

  const { addFunds, setAmountToAdd, amountToAdd } = useAddFunds();
  const { handleSetSwapFee, setSwapFee, swapFee } = useSetSwapFee();
  const { swapFees, isLoading, isError } = useGetSwapFees();
  const { claimDexRewards } = useClaimDexRewards();

  const paginatedAdmins = admins.slice(
    (adminsPage - 1) * itemsPerPage,
    adminsPage * itemsPerPage
  );

  const totalAdminPages = Math.ceil(admins.length / itemsPerPage);

  const paginatedBanned = bannedUsers.slice(
    (bannedPage - 1) * itemsPerPage,
    bannedPage * itemsPerPage
  );

  const paginatedUsers = users.slice(
    (usersPage - 1) * itemsPerPage,
    usersPage * itemsPerPage
  );

  const totalBannedPages = Math.ceil(bannedUsers.length / itemsPerPage);
  const totalUserPages = Math.ceil(users.length / itemsPerPage);

  useEffect(() => {
    getUsers()
      .then((res) => {
        setUsers(res.data);
      });
  }, []);


  // const localAdmin = admins.filter((admin: string) => admin === user?.walletAddress);
  // if (localAdmin.length === 0 ) {
  //   return (
  //     <p>No access !</p>
  //   );
  // }

  return (
    <Layout>
      <div className="flex w-full flex-col">
        <h1 className="text-3xl md:px-8">Admin dashboard</h1>
        <br></br>
        <p className="md:px-8">
          Here you can manage the DEX by adding funds, updating the swap fee,
          claiming the DEX's fees, and managing the list of admins and banned
          users.
        </p>
        <br></br>
        <main className="flex flex-1 flex-col gap-4 px-4 md:gap-8 md:px-8">
          <div className="grid gap-4 md:grid-cols-2 md:gap-8 lg:grid-cols-3">
            <div className="col-span-1">
              <CardItem title="Add ETH to DEX" icon={DollarSign}>
                <div className="flex justify-between gap-6">
                  <Input
                    type="search"
                    placeholder={amountToAdd}
                    onChange={(e) => {
                      setAmountToAdd(e.target.value);
                    }}
                  />
                  <Button onClick={addFunds}>Add</Button>
                </div>
                <p className="pt-1 text-xs text-muted-foreground">
                  If necessary, you can add more ETH to the DEX.
                </p>
              </CardItem>
            </div>
            <div className="col-span-1">
              <CardItem title="Claim DEX's Fees" icon={Trophy}>
                <Button onClick={claimDexRewards}>Claim</Button>
                <p className="pt-1 text-xs text-muted-foreground">
                  You can claim the fees that the DEX has generated.
                </p>
              </CardItem>
            </div>
            <div className="col-span-1">
              <CardItem
                title={`Current Dex's fee : ${Number(swapFees) / 100}%`}
                icon={DollarSign}
              >
                <div className="flex justify-between gap-6">
                  <Input
                    type="search"
                    placeholder={swapFee}
                    onChange={(e) => {
                      setSwapFee(e.target.value * 100);
                    }}
                  />
                  <Button onClick={handleSetSwapFee}>Update</Button>
                </div>
                <p className="pt-1 text-xs text-muted-foreground">
                  If necessary, you can update DEX&apos;s fee
                </p>
              </CardItem>
            </div>
          </div>
          <div className="grid gap-4 md:gap-8 lg:grid-cols-2 xl:grid-cols-4">
            <UserList
              title="Admin list"
              icon={UserPlus}
              users={paginatedAdmins}
              page={adminsPage}
              setPage={setAdminsPage}
              totalPages={totalAdminPages}
              firstButtonLabel="Add"
              buttonLabel="Delete"
            />
            <UserList
              title="Banned users list"
              icon={Ban}
              users={paginatedBanned}
              page={bannedPage}
              setPage={setBannedPage}
              totalPages={totalBannedPages}
              firstButtonLabel="Ban"
              buttonLabel="Unban"
            />
            {user?.role === "admin" && (
            <UserList
              title="Users"
              icon={Users}
              users={paginatedUsers}
              page={bannedPage}
              setPage={setUsersPage}
              totalPages={totalUserPages}
              firstButtonLabel="Edit"
              buttonLabel="Delete"
            />
            )}
            <MoreStats
              open={open}
              setOpen={setOpen}
              value={value}
              setValue={setValue}
              addresses={admins}
            />
          </div>
        </main>
      </div>
    </Layout>
  );
};

export default Admin;