"use client";
import Layout from "@/components/Layout/Layout";
import React, { useState } from "react";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import useStaking from "@/hooks/useStaking";
import useUnStaking from "@/hooks/useUnStaking";
import { useToast } from "@/components/ui/use-toast";
import { ethers } from "ethers"; // Pour convertir Wei en Ether pour l'affichage
import useClaimStakingRewards from "@/hooks/useClaimStakingRewards";
import useGetAllETHStaked from "@/hooks/useGetAllETHStaked";
import useGetTotalStakers from "@/hooks/useGetTotalStakers";
import useGetAmountStaked from "@/hooks/useGetAmountStaked";

const Staking = () => {
  const { stake, setAmountToStake, amountToStake } = useStaking();
  const { unStake, setAmountToUnStake, amountToUnStake } = useUnStaking();
  const { claimStakingRewards } = useClaimStakingRewards();

  const getAllEthStaked = useGetAllETHStaked();
  const getTotalStakers = useGetTotalStakers();
  const getAmountStaked = useGetAmountStaked();

  const totalStakersFormatted = Number(getTotalStakers.totalStakers);
  const amountStakedFormatted =
    Number(getAmountStaked.amountStaked) / Math.pow(10, 18).toString();

  return (
    <Layout>
      <div className="md:px-8">
        <h1 className="text-3xl">Earn ETH by staking ETH</h1>
        <br></br>
        <p>
          Staking is the process of actively participating in transaction
          validation (similar to mining) on a proof-of-stake (PoS) blockchain.
          Anyone with a minimum-required balance of a specific cryptocurrency
          can validate transactions and earn Staking rewards.
        </p>
        <br></br>
        <div className="grid grid-cols-3 gap-5">
          <Card>
            <CardHeader>
              <CardTitle>Stake my ETH</CardTitle>
              <CardDescription>
                I already staked : {amountStakedFormatted} ETH
              </CardDescription>
            </CardHeader>
            <CardContent>
              <div className="flex w-full max-w-sm items-center space-x-2">
                <Input
                  type="number"
                  placeholder="0.0000"
                  min="0.0001"
                  step="0.0001"
                  value={amountToStake}
                  onChange={(e) => setAmountToStake(e.target.value)}
                />
                <Button type="submit" onClick={stake}>
                  Stake
                </Button>
              </div>
            </CardContent>
            <CardFooter>
              <span className="text-sm">
                The more time you stake the more rewards you get
              </span>
            </CardFooter>
          </Card>
          <Card>
            <CardHeader>
              <CardTitle>Unstake or claim my ETH</CardTitle>
              <CardDescription>Unstake ≠ Claim rewards</CardDescription>
            </CardHeader>
            <CardContent>
              <div className="flex w-full max-w-sm items-center space-x-2">
                <Input
                  type="number"
                  placeholder={amountToUnStake}
                  min="0.0001"
                  step="0.0001"
                  value={amountToUnStake}
                  onChange={(e) => setAmountToUnStake(e.target.value)}
                />
                <Button type="submit" onClick={unStake}>
                  Unstake
                </Button>
                <Button type="submit" onClick={claimStakingRewards}>
                  Claim
                </Button>
              </div>
            </CardContent>
            <CardFooter>
              <span className="text-sm">
                Unstake your ETH and get your rewards
              </span>
            </CardFooter>
          </Card>
          <Card>
            <CardHeader>
              <CardTitle>Some infos</CardTitle>
            </CardHeader>
            <CardContent>
              Total ETH staked :{" "}
              {ethers.formatEther(getAllEthStaked.totalStakedAmount)} ETH
            </CardContent>
            <CardContent>
              Total Stakers : {totalStakersFormatted} Users
            </CardContent>
          </Card>
        </div>
      </div>
    </Layout>
  );
};

export default Staking;
