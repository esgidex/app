import Layout from "@/components/Layout/Layout";
import { useState } from "react";
import HeaderComponent from "@/components/tokenlist/HeaderComponent";
import PaginationComponent from "@/components/tokenlist/PaginationComponent";
import { Skeleton } from "@/components/ui/skeleton";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { Card } from "@/components/ui/card";
import { useTokenList } from "@/hooks/useTokenList";
import TokenDetails from "@/components/tokenlist/TokenDetails";

const TokenListPage = () => {
  const { allTokensAddress, isLoading, isError } = useTokenList();
  const [currentPage, setCurrentPage] = useState(1);
  const tokensPerPage = 10;

  const indexOfLastToken = currentPage * tokensPerPage;
  const indexOfFirstToken = indexOfLastToken - tokensPerPage;
  const currentTokens = allTokensAddress.slice(
    indexOfFirstToken,
    indexOfLastToken
  );
  const totalPages = Math.ceil(allTokensAddress.length / tokensPerPage);

  const handlePageChange = (pageNumber: number) => setCurrentPage(pageNumber);

  if (isLoading)
    return (
      <Layout>
        <h1 className="text-3xl md:px-8">Tokens</h1>
        <br></br>
        <p className="md:px-8">
          Here you can see the list of all the tokens available on the DEX.
        </p>
        <Table>
          <TableHeader>
            <TableRow>
              <TableHead className="w-[100px]">CA</TableHead>
              <TableHead className="text-right">Name</TableHead>
              <TableHead className="text-right">Symbol</TableHead>
              <TableHead className="text-right">Price (EUR)</TableHead>
              <TableHead className="text-right">Price (USD)</TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {Array.from({ length: tokensPerPage }).map((_, index) => (
              <TableRow key={index}>
                <TableCell className="font-medium">
                  <Skeleton className="h-4 w-full" />
                </TableCell>
                <TableCell className="text-right">
                  <Skeleton className="h-4 w-full" />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Layout>
    );

  if (isError) return <div>Error loading tokens</div>;

  return (
    <Layout>
      <div className="md:px-8">
        <h1 className="text-3xl">Tokens</h1>
        <br></br>
        <p>Here you can see the list of all the tokens available on the DEX.</p>
        <Card
          x-chunk="dashboard-01-chunk-5"
          className="xl:col-span-2 p-6 mt-12 "
        >
          <Table className="md:px-8">
            <TableHeader>
              <TableRow>
                <TableHead className="text-left">Name</TableHead>
                <TableHead className="text-right">Contract address</TableHead>
                <TableHead className="text-right">Symbol</TableHead>
                <TableHead className="text-right">Price (EUR)</TableHead>
                <TableHead className="text-right">Price (USD)</TableHead>
              </TableRow>
            </TableHeader>
            <TableBody>
              {currentTokens.map((tokenAddress) => (
                <TokenDetails
                  key={tokenAddress}
                  tokenAddress={tokenAddress as `0x${string}`}
                />
              ))}
            </TableBody>
          </Table>
        </Card>
        {/* </div> */}
        <div className="mt-6 p-6">
          <PaginationComponent
            currentPage={currentPage}
            totalPages={totalPages}
            onPageChange={handlePageChange}
          />
        </div>
      </div>
    </Layout>
  );
};

export default TokenListPage;
