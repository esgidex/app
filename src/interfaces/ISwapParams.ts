export interface ISwapParams {
  poolAddress: `0x${string}`;
  tokenA: `0x${string}`;
  tokenB: `0x${string}`;
  amountIn?: string;
}
