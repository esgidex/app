export interface IPoolData {
  tokenAAddress: `0x${string}`;
  tokenBAddress: `0x${string}`;
  tokenAName: string;
  tokenASymbol: string;
  tokenBName: string;
  tokenBSymbol: string;
  totalLiquidity: string;
  reserveA: string;
  reserveB: string;
}
