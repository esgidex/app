export const formatCryptoNumber = (
  value: string,
  decimals: number = 18,
  precision: number = 2
) => {
  const num = BigInt(value);
  const divisor = BigInt(10 ** decimals);
  const integerPart = num / divisor;
  const fractionalPart = num % divisor;
  const fractionalString = fractionalPart
    .toString()
    .padStart(decimals, "0")
    .slice(0, precision);

  return `${integerPart}.${fractionalString}`;
};

export const formatWeiToEther = (wei: any) => {
  return (wei / 10n ** 18n).toString();
};
