export const formatAddress = (address: string | undefined) => {
    if (address?.length <= 6) {
      return address;
    }
    const start = address?.slice(0, 5);
    const end = address?.slice(-4);
    return `${start}.......${end}`;
  };
  