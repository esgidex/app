import React, { createContext, ReactNode, useContext, useState, useEffect } from 'react';
import { User } from '@/hooks/useUsersApi';

const UserContext = createContext<{ user: null | User; setUser: React.Dispatch<React.SetStateAction<null>>; } > ({ user: null, setUser: () => null });

export const UserProvider = ({ children } : {children: ReactNode}) => {
    const [user, setUser] = useState(null);

    useEffect(() => {
        const user = localStorage.getItem('user');
        if (user) {
            setUser(JSON.parse(user));
        }
    }, []);

    return (
        <UserContext.Provider value={{ user, setUser }}>
            {children}
        </UserContext.Provider>
    );
};

export const useUser = () => useContext(UserContext);
