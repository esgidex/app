import { addressStaking } from "@/utils/address";
import { useWriteContract, useReadContract, useAccount } from "wagmi";
import { abi } from "@/utils/abi/StakingAbi";
import { useEffect, useState } from "react";

const useGetAllETHStaked = () => {
  const [totalStakedAmount, setTotalStakedAmount] = useState<any>(0.0);
  const {
    data: getAllETHStaked,
    isError,
    isLoading,
    isSuccess,
  } = useReadContract({
    abi,
    address: addressStaking,
    functionName: "getAllEthStaked",
  });
  const { address } = useAccount();

  useEffect(() => {
    if (!isLoading && !isError && address) {
      setTotalStakedAmount(getAllETHStaked);
    }
  }, [isError, isLoading, getAllETHStaked, address]);

  return {
    totalStakedAmount,
  };
};

export default useGetAllETHStaked;
