import { useWriteContract } from "wagmi";
import ESGIDexABI from "@/utils/abi/ESGIDex.json";
import { addressESGIDEX } from "@/utils/address";
import { useEffect, useState } from "react";

const useClaimDexRewards = () => {
  const [error, setError] = useState<string | null>(null);

  const {
    writeContract,
    isSuccess,
    isError,
    error: writeError,
  } = useWriteContract();

  const claimDexRewards = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      await writeContract({
        address: addressESGIDEX,
        abi: ESGIDexABI,
        functionName: "claimFees",
      });
      console.log("isSuccess", isSuccess);
      console.log("isError", isError);
      console.log("error", error);
    } catch (err: any) {
      setError(err.message);
    }
  };
  useEffect(() => {
    if (isError && writeError) {
      setError(writeError.message);
    }
  }, [isError, writeError]);

  return {
    claimDexRewards,
    isSuccess,
    isError,
    writeError,
    error,
  };
};

export default useClaimDexRewards;
