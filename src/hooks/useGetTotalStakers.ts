import { addressStaking } from "@/utils/address";
import { useReadContract, useAccount } from "wagmi";
import { abi } from "@/utils/abi/StakingAbi";
import { useEffect, useState } from "react";

const useGetTotalStakers = () => {
  const [totalStakers, setTotalStakers] = useState<any>(0.0);
  const {
    data: getTotalStaker,
    isError,
    isLoading,
    isSuccess,
  } = useReadContract({
    abi,
    address: addressStaking,
    functionName: "getTotalStaker",
  });
  const { address } = useAccount();

  useEffect(() => {
    if (!isLoading && !isError && address) {
      setTotalStakers(getTotalStaker);
    }
  }, [isError, isLoading, getTotalStaker, address]);

  return {
    totalStakers,
  };
};

export default useGetTotalStakers;
