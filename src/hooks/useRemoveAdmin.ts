import { useState, useEffect } from "react";
import { useWriteContract } from "wagmi";
import ESGIDexABI from "@/utils/abi/ESGIDex.json";
import { addressESGIDEX } from "@/utils/address";

const useRemoveAdmin = () => {
  const [adminAddress, setAdminAddress] = useState("");
  const [isRemoving, setIsRemoving] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  const {
    writeContract,
    isSuccess,
    isError,
    error: writeError,
  } = useWriteContract();

  const handleRemoveAdmin = (address: string) => {
    setIsRemoving(true);
    setError(null);
    try {
      writeContract({
        address: addressESGIDEX,
        abi: ESGIDexABI,
        functionName: "removeAdmin",
        args: [address],
      });
    } catch (err: any) {
      setError(err.message);
    } finally {
      setIsRemoving(false);
    }
  };

  useEffect(() => {
    if (isError && writeError) {
      setError(writeError.message);
    }
  }, [isError, writeError]);

  return {
    adminAddress,
    setAdminAddress,
    handleRemoveAdmin,
    isRemoving,
    isSuccess,
    isError,
    error,
  };
};

export default useRemoveAdmin;
