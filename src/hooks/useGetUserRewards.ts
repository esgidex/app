import { useReadContract } from 'wagmi';
import LiquidityPoolABI from '@/utils/abi/LiquidityPoolAbi.json';
import { Abi } from 'viem';

const useGetUserRewards = (poolAddress: `0x${string}`, userAddress: `0x${string}`) => {
  const { data: rewardsData, isLoading, isError, error, refetch } = useReadContract({
    address: poolAddress,
    abi: LiquidityPoolABI as Abi,
    functionName: 'getUserRewards',
    args: [userAddress],
  });

  const totalRewards = rewardsData ? rewardsData.toString() : '0';
  console.log("🚀 ~ useGetUserRewards ~ totalRewards:", totalRewards)

  return {
    totalRewards,
    isLoading,
    isError,
    error,
    refetch,
  };
};

export default useGetUserRewards;
