import { useReadContract } from "wagmi";
import ERC20ABI from "@/utils/abi/ERC20Abi.json";
import { Abi } from "viem";

interface CheckAllowanceParams {
  tokenAddress: `0x${string}`;
  ownerAddress: `0x${string}`;
  spenderAddress: `0x${string}`;
}

const useCheckAllowance = ({
  tokenAddress,
  ownerAddress,
  spenderAddress,
}: CheckAllowanceParams) => {
  const {
    data: allowance,
    isLoading,
    isError,
    error,
    refetch,
  } = useReadContract({
    address: tokenAddress,
    abi: ERC20ABI as Abi,
    functionName: "allowance",
    args: [ownerAddress, spenderAddress],
  });

  return {
    allowance: allowance as BigInt | undefined,
    isLoading,
    isError,
    error,
    refetch,
  };
};

export default useCheckAllowance;
