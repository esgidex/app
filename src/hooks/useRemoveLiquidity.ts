import { useWriteContract } from "wagmi";
import LiquidityPoolABI from "@/utils/abi/LiquidityPoolAbi.json";
import { Abi } from "viem";

interface RemoveLiquidityParams {
  poolAddress: `0x${string}`;
}

const useRemoveLiquidity = ({ poolAddress }: RemoveLiquidityParams) => {
  const { writeContract, isError, isPending, isSuccess } = useWriteContract();

  const handleRemoveLiquidity = async () => {
    try {
      await writeContract({
        address: poolAddress,
        abi: LiquidityPoolABI as Abi,
        functionName: "removeLiquidity",
      });
    } catch (err: any) {
      console.error("Error removing liquidity:", err);
    }
  };

  return {
    handleRemoveLiquidity,
    isLoading: isPending,
    isSuccess,
    isError,
  };
};

export default useRemoveLiquidity;
