import { useState } from "react";
import { useWriteContract, useReadContract } from "wagmi";
import LiquidityPoolABI from "@/utils/abi/LiquidityPoolAbi.json";
import { Abi } from "viem";

interface AddLiquidityParams {
  poolAddress: `0x${string}`;
}

const useAddLiquidity = ({ poolAddress }: AddLiquidityParams) => {
  const [amountA, setAmountA] = useState<string>("");
  const [amountB, setAmountB] = useState<string>("");

  console.log("useAddLiquidity hook");
  console.log("Amount A:", amountA);
  console.log("Amount B:", amountB);
  const parsedAmountA = Math.floor(Number(amountA) * Math.pow(10, 18));
  const parsedAmountB = Math.floor(Number(amountB) * Math.pow(10, 18));

  console.log("Parsed Amount A:", parsedAmountA, typeof parsedAmountA);
  console.log("Parsed Amount B:", parsedAmountB, typeof parsedAmountB);

  const { writeContract, isError, isPending, isSuccess } = useWriteContract();

  const handleAddLiquidity = async () => {
    console.log(
      "Starting addLiquidity transaction with values:",
      parsedAmountA,
      parsedAmountB
    );

    try {
      await writeContract({
        address: poolAddress,
        abi: LiquidityPoolABI as Abi,
        functionName: "addLiquidity",
        args: [parsedAmountA, parsedAmountB],
      });
    } catch (err: any) {
      console.error("Error adding liquidity:", err);
    }
  };

  const { refetch: refetchLiquidity } = useReadContract({
    address: poolAddress,
    abi: LiquidityPoolABI,
    functionName: "getReserves",
  });

  return {
    amountA,
    setAmountA,
    amountB,
    setAmountB,
    handleAddLiquidity,
    isLoading: isPending,
    isSuccess,
    isError,
  };
};

export default useAddLiquidity;
