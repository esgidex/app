import { useState, useEffect } from "react";
import { useWriteContract } from "wagmi";
import ESGIDexABI from "@/utils/abi/ESGIDex.json";
import { addressESGIDEX } from "@/utils/address";

const useSetSwapFee = () => {
  const [swapFee, setSwapFee] = useState<any>(0);
  const [error, setError] = useState<string | null>(null);

  const {
    data,
    writeContract,
    isSuccess,
    isError,
    error: writeError,
  } = useWriteContract();

  const handleSetSwapFee = async (e: React.FormEvent) => {
    console.log("swapFee", swapFee);
    console.log("addressESGIDEX", addressESGIDEX);
    console.log("ESGIDexABI", ESGIDexABI);
    e.preventDefault();
    setError(null);
    try {
      await writeContract({
        address: addressESGIDEX,
        abi: ESGIDexABI,
        functionName: "setSwapFee",
        args: [swapFee],
      });
    } catch (err: any) {
      setError(err.message);
    }
  };

  useEffect(() => {
    if (writeError) {
      setError(writeError.message);
    }
  }, [writeError]);

  return {
    swapFee,
    setSwapFee,
    handleSetSwapFee,
    isSuccess,
    isError,
    error,
  };
};

export default useSetSwapFee;
