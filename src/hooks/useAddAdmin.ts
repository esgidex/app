import { useState, useEffect } from "react";
import { useWriteContract } from "wagmi";
import ESGIDexABI from "@/utils/abi/ESGIDex.json";
import { addressESGIDEX } from "@/utils/address";

const useAddAdmin = () => {
  const [adminAddress, setAdminAddress] = useState<string>("");
  const [isCreating, setIsCreating] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  const {
    data,
    writeContract,
    isSuccess,
    isError,
    error: writeError,
  } = useWriteContract();

  const handleCreateAdmin = async (e: React.FormEvent) => {
    e.preventDefault();
    setIsCreating(true);
    setError(null);
    try {
      await writeContract({
        address: addressESGIDEX,
        abi: ESGIDexABI,
        functionName: "addAdmin",
        args: [adminAddress],
      });
    } catch (err: any) {
      setError(err.message);
    } finally {
      setIsCreating(false);
    }
  };

  useEffect(() => {
    if (writeError) {
      setError(writeError.message);
    }
  }, [writeError]);

  return {
    adminAddress,
    setAdminAddress,
    handleCreateAdmin,
    isCreating,
    isSuccess,
    isError,
    error,
  };
};

export default useAddAdmin;
