import { addressStaking } from "@/utils/address";
import { useReadContract, useAccount } from "wagmi";
import { abi } from "@/utils/abi/StakingAbi";
import { useEffect, useState } from "react";

const useGetAmountStaked = () => {
  const [amountStaked, setAmountStaked] = useState<any>(0.0);
  const { address } = useAccount();
  const {
    data: getAmountStaked,
    isError,
    isLoading,
    isSuccess,
  } = useReadContract({
    abi,
    address: addressStaking,
    functionName: "getAmountStaked",
    args: [address],
  });

  useEffect(() => {
    if (!isLoading && !isError && address) {
      setAmountStaked(getAmountStaked);
    }
  }, [isError, isLoading, getAmountStaked, address]);

  return {
    amountStaked,
  };
};

export default useGetAmountStaked;
