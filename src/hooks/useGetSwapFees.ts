import ESGIDexABI from "@/utils/abi/ESGIDex.json";
import { useEffect, useState } from "react";
import { useReadContract } from "wagmi";
import { addressESGIDEX } from "@/utils/address";

const contractAddress = addressESGIDEX;

export function useGetSwapFees() {
  const [swapFees, setSwapFees] = useState<string | number>();
  const { data, isError, isLoading } = useReadContract({
    address: contractAddress,
    abi: ESGIDexABI,
    functionName: "getSwapFee",
  });

  useEffect(() => {
    if (data) {
      setSwapFees(data as string | number);
    }
  }, [data]);

  return { swapFees, isLoading, isError };
}
