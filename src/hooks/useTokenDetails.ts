import { useReadContract } from 'wagmi';
import ERC20ABI from '@/utils/abi/ERC20Abi.json';

const useTokenDetails = (tokenAddress: `0x${string}`) => {
  const { data: tokenName } = useReadContract({
    address: tokenAddress,
    abi: ERC20ABI,
    functionName: 'name',
  });

  const { data: tokenSymbol } = useReadContract({
    address: tokenAddress,
    abi: ERC20ABI,
    functionName: 'symbol',
  });

  return {
    name: tokenName as string ?? '',
    symbol: tokenSymbol as string ?? ''
  };
};

export default useTokenDetails;
