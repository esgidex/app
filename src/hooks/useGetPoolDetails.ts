import { useReadContract } from 'wagmi';
import LiquidityPoolABI from '@/utils/abi/LiquidityPoolAbi.json';
import ERC20ABI from '@/utils/abi/ERC20Abi.json';
import { IPoolData } from '@/interfaces/ILiquidityPool';

const useGetPoolDetails = (poolAddress: `0x${string}`): IPoolData => {
  const { data: tokenA } = useReadContract({
    address: poolAddress,
    abi: LiquidityPoolABI,
    functionName: 'tokenA',
  });

  const { data: tokenB } = useReadContract({
    address: poolAddress,
    abi: LiquidityPoolABI,
    functionName: 'tokenB',
  });

  const { data: tokenAName } = useReadContract({
    address: tokenA as `0x${string}`,
    abi: ERC20ABI,
    functionName: 'name',
  });

  const { data: tokenBName } = useReadContract({
    address: tokenB as `0x${string}`,
    abi: ERC20ABI,
    functionName: 'name',
  });

  const { data: tokenASymbol } = useReadContract({
    address: tokenA as `0x${string}`,
    abi: ERC20ABI,
    functionName: 'symbol',
  });

  const { data: tokenBSymbol } = useReadContract({
    address: tokenB as `0x${string}`,
    abi: ERC20ABI,
    functionName: 'symbol',
  });

  const { data: totalLiquidity } = useReadContract({
    address: poolAddress,
    abi: LiquidityPoolABI,
    functionName: 'totalLiquidity',
  });

  const { data: reserves } = useReadContract({
    address: poolAddress,
    abi: LiquidityPoolABI,
    functionName: 'getReserves',
  });

  const reserveA = reserves ? (reserves as [BigInt, BigInt])[0] : BigInt(0);
  const reserveB = reserves ? (reserves as [BigInt, BigInt])[1] : BigInt(0);

  return {
    tokenAAddress: tokenA as `0x${string}`,
    tokenBAddress: tokenB as `0x${string}`,
    tokenAName: tokenAName as string ?? '',
    tokenBName: tokenBName as string ?? '',
    tokenASymbol: tokenASymbol as string ?? '',
    tokenBSymbol: tokenBSymbol as string ?? '',
    totalLiquidity: totalLiquidity ? totalLiquidity.toString() : '0',
    reserveA: reserveA.toString(),
    reserveB: reserveB.toString(),
  };
};

export default useGetPoolDetails;
