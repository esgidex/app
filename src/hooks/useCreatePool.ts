import { useState, useEffect } from "react";
import { useWriteContract, useWatchContractEvent } from "wagmi";
import LiquidityPoolFactoryABI from "@/utils/abi/LiquidityPoolFactoryAbi.json";
import { addressPoolFactory } from "@/utils/address";

const useCreatePool = () => {
  const [tokenA, setTokenA] = useState<string>("");
  const [tokenB, setTokenB] = useState<string>("");
  const [priceFeedA, setPriceFeedA] = useState<string>("");
  const [priceFeedB, setPriceFeedB] = useState<string>("");
  const [isCreating, setIsCreating] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [newPoolAddress, setNewPoolAddress] = useState<`0x${string}` | null>(null);

  const {
    writeContract: createPoolContract,
    isSuccess: isCreateSuccess,
    isError: isCreateError,
    error: createError,
  } = useWriteContract();

  useWatchContractEvent({
    address: addressPoolFactory,
    abi: LiquidityPoolFactoryABI,
    eventName: "PoolCreated",
    onLogs: async (event: any) => {
      try {
        const poolAddress = event[0]?.args?.poolAddress as `0x${string}`;
        if (!poolAddress) {
          throw new Error("Pool address not found in event args");
        }
        setNewPoolAddress(poolAddress);
        setIsSuccess(true);
      } catch (err: any) {
        setError(err.message);
      }
    },
    onError: (err: any) => {
      setError(err.message);
    },
  });

  const handleCreatePool = async (e: React.FormEvent) => {
    e.preventDefault();
    setIsCreating(true);
    setError(null);
    setIsSuccess(false);

    try {
      await createPoolContract({
        address: addressPoolFactory,
        abi: LiquidityPoolFactoryABI,
        functionName: "createPool",
        args: [tokenA, tokenB, priceFeedA, priceFeedB],
      });
    } catch (err: any) {
      setError(err.message);
      setIsCreating(false);
    }
  };

  useEffect(() => {
    if (createError) {
      setError(createError.message);
    }
  }, [createError]);

  useEffect(() => {
    if (isCreateSuccess) {
      console.log("Pool created successfully");
    }
    if (isCreateError) {
      console.error("Error creating pool");
    }
    setIsCreating(false);
  }, [isCreateSuccess, isCreateError]);

  return {
    tokenA,
    setTokenA,
    tokenB,
    setTokenB,
    priceFeedA,
    setPriceFeedA,
    priceFeedB,
    setPriceFeedB,
    handleCreatePool,
    isCreating,
    isSuccess,
    isError: !!error,
    error,
    newPoolAddress,
  };
};

export default useCreatePool;
