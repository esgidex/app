import { useEffect, useState } from 'react';
import { useReadContract } from 'wagmi';
import LiquidityPoolFactoryABI from '@/utils/abi/LiquidityPoolFactoryAbi.json';
import { addressPoolFactory } from '@/utils/address';
import { IPoolData } from '@/interfaces/ILiquidityPool';

const contractAddress = addressPoolFactory;

export function useGetAllPools() {
  const [allPools, setAllPools] = useState<string[]>([]);
  const { data, isError, isLoading } = useReadContract({
    address: contractAddress,
    abi: LiquidityPoolFactoryABI,
    functionName: 'getAllPools',
  });

  useEffect(() => {
    if (data) {
      console.log("Fetched Pools Data:", data);
      setAllPools(data as string[]);
    }
  }, [data]);

  return { allPools, isLoading, isError };
}
