import { useState, useEffect } from 'react';
import { useReadContract } from 'wagmi';
import ESGIDexABI from "@/utils/abi/ESGIDex.json";
import { addressESGIDEX } from "@/utils/address";

const useGetAllAdmins = () => {
  const [admins, setAdmins] = useState<string[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  const { data, isError, error: readError } = useReadContract({
    address: addressESGIDEX,
    abi: ESGIDexABI,
    functionName: 'getAdmins',
  });

  useEffect(() => {
    if (data) {
      setAdmins(data as string[]);
      setIsLoading(false);
    } else if (isError) {
      setError(readError?.message || 'Failed to fetch admins');
      setIsLoading(false);
    }
  }, [data, isError, readError]);

  return { admins, isLoading, error };
};

export default useGetAllAdmins;
