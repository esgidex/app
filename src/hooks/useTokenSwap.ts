import { useState, useEffect } from 'react';
import { useWriteContract } from 'wagmi';
import ESGIDexABI from '@/utils/abi/ESGIDex.json';
import { Abi } from 'viem';
import { addressESGIDEX } from '@/utils/address';

interface UseTokenSwapParams {
  tokenIn: `0x${string}`;
  tokenOut: `0x${string}`;
  amountIn: bigint;
}

const useTokenSwap = (params?: UseTokenSwapParams) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);
  const [error, setError] = useState<any>(null);

  const { writeContract, isPending: writeLoading, isSuccess: writeSuccess, isError: writeError, error: writeErrorMsg } = useWriteContract();

  const executeSwap = async () => {
    if (!params) {
      setIsError(true);
      setError('Params are not defined');
      return;
    }

    try {
      setIsLoading(true);
      setIsSuccess(false);
      setIsError(false);
      setError(null);

      await writeContract({
        address: addressESGIDEX,
        abi: ESGIDexABI as Abi,
        functionName: 'swap',
        args: [params.tokenIn, params.tokenOut, params.amountIn],
      });
    } catch (err) {
      setIsError(true);
      setError(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (writeError) {
      setIsError(true);
      setError(writeErrorMsg);
    } else if (writeSuccess) {
      setIsSuccess(true);
    }
  }, [writeError, writeSuccess, writeErrorMsg]);

  return {
    isLoading: isLoading || writeLoading,
    isSuccess,
    isError,
    error,
    executeSwap,
  };
};

export default useTokenSwap;
