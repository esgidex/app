import { useState, useEffect } from "react";
import { useWriteContract } from "wagmi";
import ESGIDexABI from "@/utils/abi/ESGIDex.json";
import { addressESGIDEX } from "@/utils/address";

const useUnbannedUser = () => {
  const [isUnbanning, setIsUnbanning] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  const {
    writeContract,
    isSuccess,
    isError,
    error: writeError,
  } = useWriteContract();

  const handleUnbannedUser = async (address: string) => {
    setIsUnbanning(true);
    setError(null);
    try {
      await writeContract({
        address: addressESGIDEX,
        abi: ESGIDexABI,
        functionName: "unbanUser",
        args: [address],
      });
    } catch (err: any) {
      setError(err.message);
    } finally {
      setIsUnbanning(false);
    }
  };

  useEffect(() => {
    if (isError && writeError) {
      setError(writeError.message);
    }
  }, [isError, writeError]);

  return {
    handleUnbannedUser,
    isUnbanning,
    isSuccess,
    isError,
    error,
  };
};

export default useUnbannedUser;
