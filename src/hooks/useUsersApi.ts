import { useCallback } from "react";
import axios from 'axios';
import { API_URL } from '@/utils/config';

export interface User {
  id?: number;
  firstName: string;
  lastName: string;
  email: string;
  password?: string;
  walletAddress: string;
  role: string;
}

interface UserApi {
  createUser: (data: User) => Promise<any>;
  getUser: (id: number) => Promise<any>;
  getUsers: () => Promise<any>;
  updateUser: (id: number, data: User) => Promise<any>;
  deleteUser: (id: number) => Promise<any>;
  login: (data: { email: string; password: string }) => Promise<any>;
}

export const useUsersApi = (): UserApi => {
  const createUser = useCallback(async (data: User) => {
    return axios.post(`${API_URL}/CreateUser`, data);
  }, []);

  const getUser = useCallback(async (id: number) => {
    return axios.get(`${API_URL}/User/${id}`);
  }, []);

  const getUsers = useCallback(async () => {
    return axios.get(`${API_URL}/AllUsers`);
  }, []);

  const updateUser = useCallback(async (id: number, data: User) => {
    return axios.put(`${API_URL}/UpdateUser/${id}`, data);
  }, []);

  const deleteUser = useCallback(async (id: number) => {
    return axios.delete(`${API_URL}/DeleteUser/${id}`);
  }, []);

  const login = useCallback(async (data: { email: string; password: string }) => {
    return axios.post(`${API_URL}/login`, data);
  }, []);

  return {
    createUser,
    getUser,
    getUsers,
    updateUser,
    deleteUser,
    login,
  };
};