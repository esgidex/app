import { useEffect, useState } from "react";
import { useReadContract } from "wagmi";
import LiquidityPoolFactoryABI from "@/utils/abi/LiquidityPoolFactoryAbi.json";
import { addressPoolFactory } from "@/utils/address";

const contractAddress = addressPoolFactory;

export function useTokenList() {
  const [allTokensAddress, setAllTokensAddress] = useState<string[]>([]);
  const [NumberOfTokens, setNumberOfTokens] = useState<number>(0);

  const { data, isError, isLoading } = useReadContract({
    address: contractAddress,
    abi: LiquidityPoolFactoryABI,
    functionName: "getAllTokens",
  });

  useEffect(() => {
    if (data) {
      setAllTokensAddress(data as string[]);
      setNumberOfTokens(data?.length);
    }
  }, [data]);

  return { NumberOfTokens, allTokensAddress, isLoading, isError };
}
