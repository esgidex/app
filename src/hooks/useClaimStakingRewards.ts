import { addressStaking } from "@/utils/address";
import { useWriteContract } from "wagmi";
import { abi } from "@/utils/abi/StakingAbi";
import { useEffect, useState } from "react";

const useClaimStakingRewards = () => {
  const [error, setError] = useState<string | null>(null);

  const {
    writeContract,
    isSuccess,
    isError,
    error: writeError,
  } = useWriteContract();

  const claimStakingRewards = async (e: React.FormEvent) => {
    e.preventDefault();
    console.log("abi", abi);
    try {
      await writeContract({
        address: addressStaking,
        abi: abi,
        functionName: "claimRewards",
      });
      console.log("isSuccess", isSuccess);
      console.log("isError", isError);
      console.log("error", error);
    } catch (err: any) {
      setError(err.message);
    }
  };
  useEffect(() => {
    if (isError && writeError) {
      setError(writeError.message);
    }
  }, [isError, writeError]);

  return {
    claimStakingRewards,
    isSuccess,
    isError,
    writeError,
    error,
  };
};

export default useClaimStakingRewards;
