import { addressStaking } from "@/utils/address";
import { useWriteContract } from "wagmi";
import { abi } from "@/utils/abi/StakingAbi";
import { useEffect, useState } from "react";

const useUnStaking = () => {
  const [amountToUnStake, setAmountToUnStake] = useState<any>(0.0002);
  const [error, setError] = useState<string | null>(null);

  const {
    writeContract,
    isSuccess,
    isError,
    error: writeError,
  } = useWriteContract();

  const unStake = async (e: React.FormEvent) => {
    e.preventDefault();
    console.log("abi", abi);
    const convertedAmount = amountToUnStake
      ? BigInt(Math.floor(Number(amountToUnStake) * Math.pow(10, 18)))
      : 0n;
    try {
      await writeContract({
        address: addressStaking,
        abi: abi,
        functionName: "unstake",
        args: [convertedAmount],
      });
      console.log("isSuccess", isSuccess);
      console.log("isError", isError);
      console.log("error", error);
    } catch (err: any) {
      setError(err.message);
    }
  };
  useEffect(() => {
    if (isError && writeError) {
      setError(writeError.message);
    }
  }, [isError, writeError]);

  return {
    unStake,
    isSuccess,
    isError,
    writeError,
    error,
    setAmountToUnStake,
    amountToUnStake,
  };
};

export default useUnStaking;
