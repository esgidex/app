import { addressStaking } from "@/utils/address";
import { useWriteContract } from "wagmi";
import { abi } from "@/utils/abi/StakingAbi";
import { useEffect, useState } from "react";

const useAddFunds = () => {
  const [amountToAdd, setAmountToAdd] = useState<any>(0.1);
  const [error, setError] = useState<string | null>(null);

  const {
    writeContract,
    isSuccess,
    isError,
    error: writeError,
  } = useWriteContract();

  const addFunds = async (e: React.FormEvent) => {
    e.preventDefault();
    const convertedAmount = amountToAdd
      ? BigInt(Math.floor(Number(amountToAdd) * Math.pow(10, 18)))
      : 0n;
    try {
      await writeContract({
        address: addressStaking,
        abi: abi,
        functionName: "addFunds",
        value: convertedAmount,
      });
      console.log("isSuccess", isSuccess);
      console.log("isError", isError);
      console.log("error", error);
    } catch (err: any) {
      setError(err.message);
    }
  };
  useEffect(() => {
    if (isError && writeError) {
      setError(writeError.message);
    }
  }, [isError, writeError]);

  return {
    addFunds,
    isSuccess,
    isError,
    writeError,
    error,
    setAmountToAdd,
    amountToAdd,
  };
};
export default useAddFunds;
