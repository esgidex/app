import { useState, useEffect } from 'react';
import { useReadContract } from 'wagmi';
import LiquidityPoolFactoryABI from '@/utils/abi/LiquidityPoolFactoryAbi.json';
import { addressPoolFactory } from '@/utils/address';

const contractAddress = addressPoolFactory;

const useGetPoolAddress = (tokenA: `0x${string}`, tokenB: `0x${string}`) => {
  const [poolAddress, setPoolAddress] = useState<`0x${string}` | null>(null);
  const [fetched, setFetched] = useState(false);

  const { data, isError, isLoading } = useReadContract({
    address: contractAddress,
    abi: LiquidityPoolFactoryABI,
    functionName: 'getPoolAddress',
    args: [tokenA, tokenB],
  });

  useEffect(() => {
    if (data && !isError && !isLoading) {
      console.log('Fetched pool address:', data);
      setPoolAddress(data as `0x${string}`);
      setFetched(true);
    }
  }, [data, isError, isLoading]);

  return { poolAddress, isError, isLoading, fetched };
};

export default useGetPoolAddress;