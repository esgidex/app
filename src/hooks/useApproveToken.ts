import { useState, useEffect } from "react";
import { useWriteContract } from "wagmi";
import ERC20ABI from "@/utils/abi/ERC20Abi.json";
import { Abi } from "viem";

interface ApproveParams {
  tokenAddress: `0x${string}`;
  spenderAddress: `0x${string}`;
}

const useApproveToken = ({ tokenAddress, spenderAddress }: ApproveParams) => {
  const [isApproving, setIsApproving] = useState(false);
  const { writeContract, isSuccess, error, isError, isPending } = useWriteContract();

  const handleApprove = async (amount: bigint) => {
    try {
      setIsApproving(true);
      await writeContract({
        address: tokenAddress,
        abi: ERC20ABI as Abi,
        functionName: "approve",
        args: [spenderAddress, amount],
      });
      console.log("Approval successful");
    } catch (err: any) {
      console.error(`Approval failed for token at ${tokenAddress}:`, err);
    } finally {
      setIsApproving(false);
    }
  };

  return {
    isLoading: isPending || isApproving,
    isSuccess,
    isError,
    error,
    handleApprove,
  };
};

export default useApproveToken;
