import { useState } from 'react';
import { useWriteContract } from 'wagmi';
import LiquidityPoolABI from '@/utils/abi/LiquidityPoolAbi.json';
import { Abi } from 'viem';

interface ClaimRewardsParams {
  poolAddress: `0x${string}`;
  refetchRewards: () => void;
}

const useClaimRewards = ({ poolAddress, refetchRewards }: ClaimRewardsParams) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { writeContract, data, isError, isSuccess, isPending } = useWriteContract();

  const handleClaimRewards = async (e: React.FormEvent) => {
    e.preventDefault();
    setIsLoading(true);
    console.log('Attempting to claim rewards...');
    try {
      const tx = await writeContract({
        address: poolAddress,
        abi: LiquidityPoolABI as Abi,
        functionName: 'claimRewards',
      });
      console.log('Transaction sent:', tx);
      refetchRewards();
    } catch (err) {
      console.error('Error initiating transaction:', err);
      setIsLoading(false);
    }
  };

  return {
    handleClaimRewards,
    isLoading: isLoading || isPending,
    isError,
    isSuccess,
  };
};

export default useClaimRewards;
