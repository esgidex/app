import { useState, useEffect } from "react";
import { useReadContract } from "wagmi";
import ESGIDexABI from "@/utils/abi/ESGIDex.json";
import { addressESGIDEX } from "@/utils/address";

const useGetAllBannedUsers = () => {
  const [bannedUsers, setBannedUsers] = useState<string[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  const {
    data,
    isError,
    error: readError,
  } = useReadContract({
    address: addressESGIDEX,
    abi: ESGIDexABI,
    functionName: "getBannedUsers",
  });

  useEffect(() => {
    if (data) {
      setBannedUsers(data as string[]);
      setIsLoading(false);
    } else if (isError) {
      setError(readError?.message || "Failed to fetch banned users");
      setIsLoading(false);
    }
  }, [data, isError, readError]);

  return { bannedUsers, isLoading, error };
};

export default useGetAllBannedUsers;
