import { useState, useEffect } from 'react';
import { useWriteContract } from 'wagmi';
import LiquidityPoolFactoryABI from '@/utils/abi/LiquidityPoolFactoryAbi.json';
import { addressPoolFactory } from '@/utils/address';

const useDeletePool = () => {
  const [isDeleting, setIsDeleting] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const { writeContract, isSuccess: writeSuccess, isError: writeError, error: contractError } = useWriteContract();

  const handleDeletePool = async (poolAddress: `0x${string}`): Promise<boolean> => {
    setIsDeleting(true);
    setError(null);
    setIsSuccess(false);
    setIsError(false);

    try {
      await writeContract({
        address: addressPoolFactory,
        abi: LiquidityPoolFactoryABI,
        functionName: 'removePool',
        args: [poolAddress],
      });
      setIsSuccess(true);
      return true;
    } catch (err: any) {
      setError(err.message);
      setIsError(true);
      return false;
    } finally {
      setIsDeleting(false);
    }
  };

  useEffect(() => {
    if (writeError && contractError) {
      setError(contractError.message);
      setIsError(true);
    }
  }, [writeError, contractError]);

  return { handleDeletePool, isDeleting, isSuccess, isError, error };
};

export default useDeletePool;
