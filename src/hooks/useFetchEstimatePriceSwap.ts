import { useReadContract } from "wagmi";
import ESGIDexABI from "@/utils/abi/ESGIDex.json";
import { Abi } from "viem";
import { addressESGIDEX } from "@/utils/address";
import { formatWeiToEther } from "@/utils/formatNumber";

interface UseFetchEstimatePriceSwapParams {
  tokenIn: `0x${string}`;
  tokenOut: `0x${string}`;
  amountIn: bigint;
}

const useFetchEstimatePriceSwap = (params?: UseFetchEstimatePriceSwapParams) => {
  const { data, isLoading, isError, error } = useReadContract({
    address: addressESGIDEX,
    abi: ESGIDexABI as Abi,
    functionName: "estimateSwap",
    args: params ? [params.tokenIn, params.tokenOut, params.amountIn] : [],
  });

  const amountOut = data && Array.isArray(data) && data.length > 0 ? formatWeiToEther(BigInt(data[0])) : '0';
  const fee = data && Array.isArray(data) && data.length > 1 ? formatWeiToEther(BigInt(data[1])) : '0';

  return {
    formattedAmountOut: amountOut,
    fee,
    isLoading,
    isError,
    error,
  };
};

export default useFetchEstimatePriceSwap;
