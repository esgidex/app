import { useState, useEffect } from "react";
import { useWriteContract } from "wagmi";
import ESGIDexABI from "@/utils/abi/ESGIDex.json";
import { addressESGIDEX } from "@/utils/address";

const useBanUser = () => {
  const [userAddress, setUserAddress] = useState("");
  const [isBanning, setIsBanning] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  const {
    writeContract,
    isSuccess,
    isError,
    error: writeError,
  } = useWriteContract();

  const handleBanUser = (e: React.FormEvent) => {
    e.preventDefault();
    setIsBanning(true);
    setError(null);
    try {
      writeContract({
        address: addressESGIDEX,
        abi: ESGIDexABI,
        functionName: "banUser",
        args: [userAddress],
      });
    } catch (err: any) {
      setError(err.message);
    } finally {
      setIsBanning(false);
    }
  };

  useEffect(() => {
    if (isError && writeError) {
      setError(writeError.message);
    }
  }, [isError, writeError]);

  return {
    userAddress,
    setUserAddress,
    handleBanUser,
    isBanning,
    isSuccess,
    isError,
    error,
  };
};

export default useBanUser;
