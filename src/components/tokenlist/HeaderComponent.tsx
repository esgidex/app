"use client";

const HeaderComponent = ({ title }: any) => (
  <div className="sm:flex sm:items-center">
    <div className="sm:flex-auto">
      <h1 className="text-2xl font-semibold leading-6 text-white">{title}</h1>
    </div>
  </div>
);

export default HeaderComponent;
