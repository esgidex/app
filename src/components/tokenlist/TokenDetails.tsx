import React from "react";
import useTokenDetails from "@/hooks/useTokenDetails";
import { TableCell, TableRow } from "@/components/ui/table";
import axios from "axios";
import { useEffect } from "react";

const TokenDetails = ({ tokenAddress }: { tokenAddress: `0x${string}` }) => {
  const { name, symbol } = useTokenDetails(tokenAddress);

  const [eur, setEur] = React.useState<number | string>("N/A");
  const [usd, setUsd] = React.useState<number | string>("N/A");

  useEffect(() => {
    axios
      .get(
        `https://min-api.cryptocompare.com/data/price?fsym=${symbol}&tsyms=USD,EUR`
      )
      .then((response) => {
        setEur(response.data.EUR);
        setUsd(response.data.USD);
      });
  }, [symbol]);

  return (
    <TableRow>
      <TableCell>{name}</TableCell>
      <TableCell className="text-right">{tokenAddress}</TableCell>
      <TableCell className="text-right">{symbol}</TableCell>
      <TableCell className="text-right">{eur} €</TableCell>
      <TableCell className="text-right">{usd} $</TableCell>
    </TableRow>
  );
};

export default TokenDetails;
