import React from 'react';
import { ClipLoader } from 'react-spinners';

const LoadingSpinner = () => (
  <div className="flex justify-center items-center">
    <ClipLoader size={50} color={"#123abc"} loading={true} />
  </div>
);

export default LoadingSpinner;
