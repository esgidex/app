import React, { useEffect, useState } from 'react';
import useGetPoolDetails from '@/hooks/useGetPoolDetails';

interface PoolDetailsComponentProps {
  poolAddress: `0x${string}`;
  tokenType: 'tokenA' | 'tokenB';
  onTokenDetailsLoaded: (tokenDetails: { name: string; symbol: string; address: `0x${string}` }) => void;
}

const PoolDetailsComponent: React.FC<PoolDetailsComponentProps> = ({ poolAddress, tokenType, onTokenDetailsLoaded }) => {
  const poolDetails = useGetPoolDetails(poolAddress);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    if (poolDetails && !isLoaded) {
      const token = tokenType === 'tokenA' ? {
        name: poolDetails.tokenAName ?? '',
        symbol: poolDetails.tokenASymbol ?? '',
        address: poolDetails.tokenAAddress as `0x${string}`,
      } : {
        name: poolDetails.tokenBName ?? '',
        symbol: poolDetails.tokenBSymbol ?? '',
        address: poolDetails.tokenBAddress as `0x${string}`,
      };

      if (token.name && token.symbol && token.address) {
        onTokenDetailsLoaded(token);
        setIsLoaded(true);
      }
    }
  }, [poolDetails, tokenType, onTokenDetailsLoaded, isLoaded]);

  return null;
};

export default PoolDetailsComponent;
