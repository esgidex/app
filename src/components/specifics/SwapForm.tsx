import React, { useState, useEffect, ChangeEvent, FormEvent } from "react";
import { useAccount } from "wagmi";
import useFetchEstimatePriceSwap from "@/hooks/useFetchEstimatePriceSwap";
import useTokenSwap from "@/hooks/useTokenSwap";
import useCheckAllowance from "@/hooks/useCheckAllowance";
import useApproveToken from "@/hooks/useApproveToken";
import { useGetAllPools } from "@/hooks/useGetAllPools";
import { Card, CardContent, CardFooter } from "@/components/ui/card";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import {
  Select,
  SelectTrigger,
  SelectValue,
  SelectContent,
  SelectItem,
} from "@/components/ui/select";
import { addressESGIDEX } from "@/utils/address";
import PoolDetailsComponent from "@/components/specifics/PoolDetailsComponent";
import { useToast } from "@/components/ui/use-toast";
import LoadingSpinner from "../LoadingSpinner";

const SwapComponent = () => {
  const { address } = useAccount();
  const { toast } = useToast();
  const [tokenIn, setTokenIn] = useState<{
    address: `0x${string}`;
    name: string;
    symbol: string;
  }>({
    address: "0x0000000000000000000000000000000000000000" as `0x${string}`,
    name: "",
    symbol: "",
  });
  const [amountIn, setAmountIn] = useState("");
  const [tokenOut, setTokenOut] = useState<{
    address: `0x${string}`;
    name: string;
    symbol: string;
  }>({
    address: "0x0000000000000000000000000000000000000000" as `0x${string}`,
    name: "",
    symbol: "",
  });
  const [tokenOptionsIn, setTokenOptionsIn] = useState<
    Array<{ address: `0x${string}`; name: string; symbol: string }>
  >([]);
  const [tokenOptionsOut, setTokenOptionsOut] = useState<
    Array<{ address: `0x${string}`; name: string; symbol: string }>
  >([]);
  const [approved, setApproved] = useState(false);
  const [approvalNeeded, setApprovalNeeded] = useState(false);

  const { allPools } = useGetAllPools();

  const parsedAmountIn = amountIn
    ? BigInt(Math.floor(Number(amountIn) * Math.pow(10, 18)))
    : 0n;

  const fetchEstimateParams =
    tokenIn.address && tokenOut.address && parsedAmountIn > 0n
      ? {
          tokenIn: tokenIn.address,
          tokenOut: tokenOut.address,
          amountIn: parsedAmountIn,
        }
      : undefined;

  const {
    formattedAmountOut,
    fee,
    isLoading: isFetchingEstimate,
    isError: isErrorEstimate,
    error: errorEstimate,
  } = useFetchEstimatePriceSwap(fetchEstimateParams);

  const swapParams =
    tokenIn.address && tokenOut.address && parsedAmountIn > 0n
      ? {
          tokenIn: tokenIn.address,
          tokenOut: tokenOut.address,
          amountIn: parsedAmountIn,
        }
      : undefined;

  const {
    isLoading: isSwapping,
    isSuccess,
    isError: isErrorSwap,
    error: errorSwap,
    executeSwap,
  } = useTokenSwap(swapParams);

  const { allowance, refetch: refetchAllowance } = useCheckAllowance({
    tokenAddress: tokenIn.address,
    ownerAddress: address as `0x${string}`,
    spenderAddress: addressESGIDEX as `0x${string}`,
  });

  const {
    handleApprove,
    isLoading: isApproving,
    isSuccess: isApproveSuccess,
    isError: isApproveError,
    error: approveError,
  } = useApproveToken({
    tokenAddress: tokenIn.address,
    spenderAddress: addressESGIDEX as `0x${string}`,
  });

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setAmountIn(e.target.value);
  };

  const handleTokenInChange = (token: {
    address: `0x${string}`;
    name: string;
    symbol: string;
  }) => {
    setTokenIn(token);
    console.log("Token In Selected:", token);
  };

  const handleTokenOutChange = (token: {
    address: `0x${string}`;
    name: string;
    symbol: string;
  }) => {
    setTokenOut(token);
    console.log("Token Out Selected:", token);
  };

  const handleTokenDetailsLoadedIn = (token: {
    address: `0x${string}`;
    name: string;
    symbol: string;
  }) => {
    setTokenOptionsIn((prevOptions) => {
      if (!prevOptions.some((opt) => opt.address === token.address)) {
        return [...prevOptions, token];
      }
      return prevOptions;
    });
  };

  const handleTokenDetailsLoadedOut = (token: {
    address: `0x${string}`;
    name: string;
    symbol: string;
  }) => {
    setTokenOptionsOut((prevOptions) => {
      if (!prevOptions.some((opt) => opt.address === token.address)) {
        return [...prevOptions, token];
      }
      return prevOptions;
    });
  };

  useEffect(() => {
    if (
      !tokenIn.address ||
      !tokenOut.address ||
      parsedAmountIn === 0n ||
      isApproving
    )
      return;

    const checkAllowance = async () => {
      if (allowance !== undefined) {
        if (BigInt(allowance.toString()) >= parsedAmountIn) {
          setApproved(true);
          setApprovalNeeded(false);
        } else {
          setApprovalNeeded(true);
        }
      } else {
        setApproved(false);
        setApprovalNeeded(false);
      }
    };

    checkAllowance();
  }, [
    allowance,
    parsedAmountIn,
    tokenIn.address,
    tokenOut.address,
    refetchAllowance,
    isApproving,
  ]);

  useEffect(() => {
    if (isApproveSuccess) {
      toast({
        title: "Approval Successful",
        description: "The token approval was successful.",
        variant: "default",
      });
      setApproved(true);
      setApprovalNeeded(false);
      refetchAllowance(); // Refresh allowance after successful approval
    } else if (isApproveError) {
      toast({
        title: "Approval Failed",
        description: approveError?.message || "The token approval failed.",
        variant: "destructive",
      });
      setApproved(false);
      setApprovalNeeded(true);
    }
  }, [isApproveSuccess, isApproveError, approveError, refetchAllowance]);

  const handleSwapWithApproval = async (e: FormEvent) => {
    e.preventDefault();
    if (!tokenIn.address || !tokenOut.address || parsedAmountIn === 0n) return;
    if (approvalNeeded) {
      await handleApprove(parsedAmountIn);
    }
    if (approved) {
      await executeSwap();
    }
  };

  useEffect(() => {
    if (isSuccess) {
      toast({
        title: "Swap Successful",
        description: "The token swap was successful.",
        variant: "default",
      });
    } else if (isErrorSwap) {
      toast({
        title: "Swap Failed",
        description: errorSwap?.message || "The token swap failed.",
        variant: "destructive",
      });
    }
  }, [isSuccess, isErrorSwap, errorSwap]);

  const isReadyToSwap =
    amountIn !== "" &&
    tokenIn.address !== "0x0000000000000000000000000000000000000000" &&
    tokenOut.address !== "0x0000000000000000000000000000000000000000" &&
    !isSwapping &&
    approved &&
    parsedAmountIn !== 0n;

  const getButtonLabel = () => {
    if (isSwapping) return "Swapping...";
    if (isApproving) return "Approving...";
    return "Swap";
  };

  return (
    <Card className="w-full max-w-md">
      <CardContent className="grid gap-6 p-6">
        {isSwapping || isApproving ? (
          <LoadingSpinner />
        ) : (
          <>
            <div className="grid grid-cols-2 gap-4">
              <div className="space-y-2">
                <Label htmlFor="tokenIn" className="text-sm font-medium">
                  From
                </Label>
                <Select
                  onValueChange={(value) =>
                    handleTokenInChange(JSON.parse(value))
                  }
                >
                  <SelectTrigger className="bg-background text-foreground border border-input rounded-md px-3 py-2 shadow-sm focus:outline-none focus:ring-2 focus:ring-primary focus:border-primary">
                    <SelectValue placeholder="Select token" />
                  </SelectTrigger>
                  <SelectContent>
                    {tokenOptionsIn.map((token) => (
                      <SelectItem
                        key={token.address}
                        value={JSON.stringify(token)}
                      >
                        {token.name} ({token.symbol})
                      </SelectItem>
                    ))}
                    {allPools.map((poolAddress) => (
                      <PoolDetailsComponent
                        key={poolAddress + "A"}
                        poolAddress={poolAddress as `0x${string}`}
                        tokenType="tokenA"
                        onTokenDetailsLoaded={handleTokenDetailsLoadedIn}
                      />
                    ))}
                  </SelectContent>
                </Select>
              </div>
              <div className="space-y-2">
                <Label htmlFor="tokenOut" className="text-sm font-medium">
                  To
                </Label>
                <Select
                  onValueChange={(value) =>
                    handleTokenOutChange(JSON.parse(value))
                  }
                >
                  <SelectTrigger className="bg-background text-foreground border border-input rounded-md px-3 py-2 shadow-sm focus:outline-none focus:ring-2 focus:ring-primary focus:border-primary">
                    <SelectValue placeholder="Select token" />
                  </SelectTrigger>
                  <SelectContent>
                    {tokenOptionsOut.map((token) => (
                      <SelectItem
                        key={token.address}
                        value={JSON.stringify(token)}
                      >
                        {token.name} ({token.symbol})
                      </SelectItem>
                    ))}
                    {allPools.map((poolAddress) => (
                      <PoolDetailsComponent
                        key={poolAddress + "B"}
                        poolAddress={poolAddress as `0x${string}`}
                        tokenType="tokenB"
                        onTokenDetailsLoaded={handleTokenDetailsLoadedOut}
                      />
                    ))}
                  </SelectContent>
                </Select>
              </div>
            </div>
            <div className="space-y-2">
              <Label htmlFor="amount" className="text-sm font-medium">
                Amount
              </Label>
              <Input
                id="amount"
                type="number"
                placeholder="0.0"
                value={amountIn}
                onChange={handleInputChange}
                className="bg-background text-foreground border border-input rounded-md px-3 py-2 shadow-sm focus:outline-none focus:ring-2 focus:ring-primary focus:border-primary"
              />
            </div>
            <div className="space-y-2">
              <Label htmlFor="received" className="text-sm font-medium">
                You will receive
              </Label>
              <Input
                id="received"
                type="text"
                placeholder="0.0"
                value={formattedAmountOut}
                disabled
                className="bg-background text-foreground border border-input rounded-md px-3 py-2 shadow-sm focus:outline-none focus:ring-2 focus:ring-primary focus:border-primary"
              />
            </div>
            <div className="space-y-2">
              <Label htmlFor="fee" className="text-sm font-medium">
                Fee : {fee / 10 ** 18} {tokenIn.symbol}
              </Label>
            </div>
          </>
        )}
      </CardContent>
      <CardFooter className="bg-muted/20 p-6">
        <Button className="w-full" onClick={handleSwapWithApproval}>
          {getButtonLabel()}
        </Button>
      </CardFooter>
    </Card>
  );
};

export default SwapComponent;
