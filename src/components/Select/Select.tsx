import React from 'react';
interface SelectInputProps {
  id: string;
  value: string;
  onChange: (value: string) => void;
  options: { value: string; label: string }[];
}

const Select = ({ id, value, onChange, options }: SelectInputProps) => {
  return (
    <div className="flex justify-between items-center bg-dark-500 p-4 rounded-xl shadow-md">
      <select
        id={id}
        value={value}
        onChange={(e) => onChange(e.target.value)}
        className="bg-transparent text-white text-lg rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2 appearance-none"
      >
        {options.map((option) => (
          <option key={option.value} value={option.value} className="bg-dark-500">
            {option.label}
          </option>
        ))}
      </select>
      <div className="text-gray-400 text-sm">
        {/* icone du token */}
        Solde : 0.002
      </div>
    </div>
  );
};

export default Select;
