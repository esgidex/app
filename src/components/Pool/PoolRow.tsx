import React from "react";
import { useAdminStatus } from "@/hooks/useAdminStatus";
import { toast } from "react-toastify";
import { formatCryptoNumber } from "@/utils/formatNumber";
import useClaimRewards from "@/hooks/useClaimRewards";
import { useAccount } from "wagmi";
import useGetUserRewards from "@/hooks/useGetUserRewards";
import useGetPoolDetails from "@/hooks/useGetPoolDetails";
import useRemoveLiquidity from "@/hooks/useRemoveLiquidity";
import { Button } from "@/components/ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { Clipboard, HandHelping, MoreHorizontal, Plus, X, MinusCircle } from "lucide-react";

interface PoolRowProps {
  poolAddress: `0x${string}`;
  onSelectPool: (poolAddress: `0x${string}`) => void;
  onDeletePool: (poolAddress: `0x${string}`) => void;
  isDeleting: boolean;
}

const PoolRow: React.FC<PoolRowProps> = ({
  poolAddress,
  onSelectPool,
  onDeletePool,
  isDeleting,
}) => {
  const poolDetails = useGetPoolDetails(poolAddress);

  if (!poolDetails) {
    return (
      <tr>
        <td colSpan={7}>Loading...</td>
      </tr>
    );
  }

  const { tokenAName, tokenBName, totalLiquidity, reserveA, reserveB } =
    poolDetails;

  const isAdmin = useAdminStatus();
  const { address } = useAccount();
  const { totalRewards, refetch: refetchRewards } = useGetUserRewards(
    poolAddress,
    address as `0x${string}`
  );
  const { handleClaimRewards, isLoading: isClaiming } = useClaimRewards({
    poolAddress,
    refetchRewards,
  });

  const { handleRemoveLiquidity, isLoading: isRemoving } = useRemoveLiquidity({ poolAddress });

  const apy = (
    ((Number(reserveA || "0") + Number(reserveB || "0")) /
      Number(totalLiquidity || "1")) *
    100
  ).toFixed(2);

  const copyToClipboard = (address: string) => {
    navigator.clipboard.writeText(address);
    toast.success("Adresse copiée dans le presse-papier");
  };

  const handleDelete = () => {
    onDeletePool(poolAddress);
  };

  return (
    <tr>
      <td className="flex pr-6 py-4 whitespace-nowrap">
        <div className="flex flex-end items-center w-full">
          <div className="flex flex-col w-2/3">
            <span className="text-sm font-medium text-white">
              {tokenAName} / {tokenBName}
            </span>
            <span className="text-xs text-gray-500 truncate">
              {poolAddress}
            </span>
          </div>
          <Button
            onClick={() => copyToClipboard(poolAddress)}
            className="items-end flex-end justify-end"
          >
            <Clipboard size={18} />
          </Button>
        </div>
      </td>
      <td className="text-start py-4 whitespace-nowrap">
        <div className="text-sm text-white">
          {formatCryptoNumber(totalLiquidity || "0")}
        </div>
      </td>
      <td className="text-start py-4 whitespace-nowrap">
        <div className="text-sm text-white">
          {formatCryptoNumber(reserveA || "0")}
        </div>
      </td>
      <td className="text-start py-4 whitespace-nowrap">
        <div className="text-sm text-white">{apy}%</div>
      </td>
      <td className="text-start py-4 whitespace-nowrap">
        <div className="text-sm text-white">
          {formatCryptoNumber(reserveB || "0")}
        </div>
      </td>
      <td className="text-start py-4 whitespace-nowrap">
        <div className="text-sm text-white">
          {formatCryptoNumber(totalRewards || "0")}
        </div>
      </td>
      <td className="text-start py-4 whitespace-nowrap text-sm font-medium">
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button
              className="text-start"
              aria-haspopup="true"
              size="icon"
              variant="ghost"
            >
              <MoreHorizontal className="h-4 w-4" />
              <span className="sr-only">Toggle menu</span>
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end">
            <DropdownMenuLabel>Actions</DropdownMenuLabel>
            <DropdownMenuItem onClick={() => onSelectPool(poolAddress)}>
              <Plus className="mr-2" size={18} />
              Add
            </DropdownMenuItem>
            <DropdownMenuItem onClick={handleClaimRewards}>
              <HandHelping className="mr-2" size={18} />
              Claim
            </DropdownMenuItem>
            <DropdownMenuItem onClick={handleRemoveLiquidity}>
              <MinusCircle className="mr-2" size={18} />
              {isRemoving ? "Removing..." : "Remove Liquidity"}
            </DropdownMenuItem>
            {isAdmin && (
              <DropdownMenuItem onClick={handleDelete}>
                <X className="mr-2" size={18} /> Remove
              </DropdownMenuItem>
            )}
          </DropdownMenuContent>
        </DropdownMenu>
      </td>
    </tr>
  );
};

export default PoolRow;
