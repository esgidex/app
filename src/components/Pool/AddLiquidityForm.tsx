import React, { useEffect, useState } from "react";
import useAddLiquidity from "@/hooks/useAddLiquidity";
import useCheckAllowance from "@/hooks/useCheckAllowance";
import useApproveToken from "@/hooks/useApproveToken";
import { useAccount } from "wagmi";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Button } from "@/components/ui/button";
interface AddLiquidityFormProps {
  poolAddress: `0x${string}`;
  tokenAAddress: `0x${string}`;
  tokenBAddress: `0x${string}`;
}

function AddLiquidityForm({
  poolAddress,
  tokenAAddress,
  tokenBAddress,
}: AddLiquidityFormProps) {
  const { address } = useAccount();
  const {
    amountA,
    setAmountA,
    amountB,
    setAmountB,
    handleAddLiquidity,
    isLoading: isAddingLiquidity,
    isSuccess,
    isError,
  } = useAddLiquidity({ poolAddress });

  const [needsApprovalA, setNeedsApprovalA] = useState<boolean>(false);
  const [needsApprovalB, setNeedsApprovalB] = useState<boolean>(false);

  const { allowance: allowanceA, refetch: refetchAllowanceA } =
    useCheckAllowance({
      tokenAddress: tokenAAddress,
      ownerAddress: address as `0x${string}`,
      spenderAddress: poolAddress,
    });

  const { allowance: allowanceB, refetch: refetchAllowanceB } =
    useCheckAllowance({
      tokenAddress: tokenBAddress,
      ownerAddress: address as `0x${string}`,
      spenderAddress: poolAddress,
    });

  const { handleApprove: handleApproveA, isLoading: isApprovingA } =
    useApproveToken({
      tokenAddress: tokenAAddress,
      spenderAddress: poolAddress,
    });

  const { handleApprove: handleApproveB, isLoading: isApprovingB } =
    useApproveToken({
      tokenAddress: tokenBAddress,
      spenderAddress: poolAddress,
    });

  useEffect(() => {
    if (
      amountA &&
      allowanceA !== undefined &&
      amountB &&
      allowanceB !== undefined
    ) {
      setNeedsApprovalA(
        BigInt(allowanceA.toString()) <
          BigInt(Math.floor(Number(amountA) * Math.pow(10, 18)))
      );
      setNeedsApprovalB(
        BigInt(allowanceB.toString()) <
          BigInt(Math.floor(Number(amountB) * Math.pow(10, 18)))
      );
    }
  }, [amountA, amountB, allowanceA, allowanceB]);

  const handleAddLiquidityWithApproval = async (e: React.FormEvent) => {
    e.preventDefault();
    console.log(
      "handleAddLiquidityWithApproval called with needsApprovalA:",
      needsApprovalA,
      "needsApprovalB:",
      needsApprovalB
    );

    try {
      // Step 1: Approve tokens if needed
      if (needsApprovalA) {
        console.log("Requesting approval for Token A");
        await handleApproveA(
          BigInt(Math.floor(Number(amountA) * Math.pow(10, 18)))
        );
        console.log("Approval for Token A successful");
      }

      if (needsApprovalB) {
        console.log("Requesting approval for Token B");
        await handleApproveB(
          BigInt(Math.floor(Number(amountB) * Math.pow(10, 18)))
        );
        console.log("Approval for Token B successful");
      }

      // Step 2: Refetch allowances
      const newAllowanceA = await refetchAllowanceA();
      const newAllowanceB = await refetchAllowanceB();

      console.log("newAllowanceA:", newAllowanceA);
      console.log("newAllowanceB:", newAllowanceB);

      // Step 3: Check if approvals are sufficient
      const updatedNeedsApprovalA =
        BigInt(newAllowanceA?.data?.toString() || "0") <
        BigInt(Number(amountA));
      const updatedNeedsApprovalB =
        BigInt(newAllowanceB?.data?.toString() || "0") <
        BigInt(Number(amountB));

      console.log(
        "Updated needsApprovalA:",
        updatedNeedsApprovalA,
        "Updated needsApprovalB:",
        updatedNeedsApprovalB
      );

      // Step 4: Add liquidity if no further approvals are needed
      if (!updatedNeedsApprovalA && !updatedNeedsApprovalB) {
        console.log("Adding liquidity with values:", amountA, amountB);
        await handleAddLiquidity();
      } else {
        console.log("Still need approvals, cannot add liquidity yet.");
      }
    } catch (error) {
      console.error("Error in approval or adding liquidity:", error);
    }
  };

  const isReadyToAddLiquidity =
    !isAddingLiquidity && !isApprovingA && !isApprovingB;

  return (
    <div className="w-full border p-12 rounded-lg">
      <form onSubmit={handleAddLiquidityWithApproval}>
        <div>
          <Label
            htmlFor="amountA"
            className="block text-white text-sm font-bold mb-2"
          >
            Amount Token A
          </Label>
          <Input
            id="amountA"
            type="text"
            value={amountA}
            placeholder="0"
            onChange={(e) => setAmountA(e.target.value)}
          />
        </div>
        <div className="mt-4">
          <Label
            htmlFor="amountB"
            className="block text-white text-sm font-bold mb-2"
          >
            Amount Token B
          </Label>
          <Input
            id="amountB"
            type="text"
            value={amountB}
            placeholder="0"
            onChange={(e) => setAmountB(e.target.value)}
          />
        </div>
        <Button
          type="submit"
          
          className="w-full mt-4"
          disabled={isAddingLiquidity || isApprovingA || isApprovingB}
        >
          {!isReadyToAddLiquidity && isAddingLiquidity
            ? "Adding..."
            : "Add Liquidity"}
        </Button>
        {isSuccess && (
          <p className="mt-2 text-green-500">Liquidity added successfully!</p>
        )}
        {isError && <p className="mt-2 text-red-500">Error: {isError}</p>}
      </form>
    </div>
  );
}

export default AddLiquidityForm;
