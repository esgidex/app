import React from "react";
import CardItem from "@/components/AdminDashboard/CardItem";
import { DollarSign, Users, BadgeDollarSign } from "lucide-react";
interface MarketDataProps {
  volume: string;
  tokens: string;
  users: string;
}

const MarketData = ({ volume, tokens, users }: MarketDataProps) => {
  return (
    <div className="grid gap-4 md:grid-cols-2 md:gap-8 lg:grid-cols-3">
      <div className="col-span-1">
        <CardItem title="Volume traded" icon={DollarSign}>
          <div className="flex justify-between gap-6"></div>
          <p className="pt-1 text-xs text-muted-foreground">
            {volume} traded since day one
          </p>
        </CardItem>
      </div>
      <div className="col-span-1">
        <CardItem title="Assets available" icon={BadgeDollarSign}>
          <p className="pt-1 text-xs text-muted-foreground">
            {tokens} are now available on ESGIDex
          </p>
        </CardItem>
      </div>
      <div className="col-span-1">
        <CardItem title="Total Users" icon={Users}>
          <p className="pt-1 text-xs text-muted-foreground">{users} users trusted us </p>
        </CardItem>
      </div>
    </div>
  );
};

export default MarketData;
