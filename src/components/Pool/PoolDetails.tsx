import React from 'react';
import useGetPoolDetails from '@/hooks/useGetPoolDetails';
import { formatCryptoNumber } from '@/utils/formatNumber';

interface PoolDetailsProps {
  poolAddress: `0x${string}`;
}

const PoolDetails: React.FC<PoolDetailsProps> = ({ poolAddress }) => {
  const poolDetails = useGetPoolDetails(poolAddress);

  if (!poolDetails) {
    return <div>Loading...</div>;
  }

  return (
    <div className="text-white">
      <span>Token A Reserve: {formatCryptoNumber(poolDetails.reserveA || '0')}</span> <br />
      <span>Token B Reserve: {formatCryptoNumber(poolDetails.reserveB || '0')}</span> <br />
      <span>Total Liquidity: {formatCryptoNumber(poolDetails.totalLiquidity || '0')}</span> <br />
    </div>
  );
};

export default PoolDetails;
