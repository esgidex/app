import React from "react";
import PoolRow from "./PoolRow";
import MarketData from "./MarketData";
import {
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableFooter,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { Separator } from "../ui/separator";
import { useTokenList } from "@/hooks/useTokenList";
interface PoolTableProps {
  pools: `0x${string}`[];
  onSelectPool: (poolAddress: `0x${string}`) => void;
  onDeletePool: (poolAddress: `0x${string}`) => void;
  isDeleting: boolean;
  deletingPools: `0x${string}`[]; // Ajout de deletingPools à l'interface
}

function PoolTable({
  pools,
  onSelectPool,
  onDeletePool,
  isDeleting,
  deletingPools,
}: PoolTableProps) {
  const { NumberOfTokens } = useTokenList();
  if (pools.length === 0) return <div>Chargement...</div>;

  const TableHead = [
    "Pairs",
    "Total Supplied",
    "Reserves Token A",
    "APY",
    "Reserves Token B",
    "Rewards",
    "Actions",
  ];

  return (
    <div className="overflow-x-auto">
      <MarketData volume="$42 ETH" tokens={`${NumberOfTokens}+`} users="125" />
      <Separator className="mt-8" />
      <Table className="min-w-full divide-y mt-12">
        <TableHeader>
          <tr>
            {TableHead.map((head) => (
              <th
                key={head}
                className="py-3 text-left text-xs font-medium text-white uppercase tracking-wider"
              >
                {head}
              </th>
            ))}
          </tr>
        </TableHeader>
        <TableBody>
          {pools.map((pool, index) => (
            <PoolRow
              key={index}
              poolAddress={pool}
              onSelectPool={onSelectPool}
              onDeletePool={onDeletePool}
              isDeleting={deletingPools.includes(pool)}
            />
          ))}
        </TableBody>
      </Table>
      <Separator />
    </div>
  );
}

export default PoolTable;
