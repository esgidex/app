import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import useDeletePool from '@/hooks/useDeletePool';
import PoolTable from './PoolTable';
import { useGetAllPools } from '@/hooks/useGetAllPools';

const AllPools = () => {
  const { allPools: pools, isError: error, isLoading } = useGetAllPools();
  const [poolList, setPoolList] = useState<`0x${string}`[]>([]);
  const [deletingPools, setDeletingPools] = useState<`0x${string}`[]>([]);

  const { handleDeletePool, isDeleting, isSuccess, isError, error: deleteError } = useDeletePool();

  useEffect(() => {
    if (pools && Array.isArray(pools)) {
      setPoolList(pools as `0x${string}`[]);
    }
  }, [pools]);

  const handleDelete = async (poolAddress: `0x${string}`) => {
    setDeletingPools((prev) => [...prev, poolAddress]);
    const success = await handleDeletePool(poolAddress);
    if (success) {
      toast.success('Pool supprimée avec succès');
      setPoolList((prevPools) => prevPools.filter((pool) => pool !== poolAddress));
    } else if (isError) {
      toast.error(`Erreur lors de la suppression de la pool : ${deleteError}`);
    }
    setDeletingPools((prev) => prev.filter((pool) => pool !== poolAddress));
  };

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error loading pools: {error}</div>;

  return (
    <div>
      <h2 className="text-white font-bold mb-4">All Liquidity Pools</h2>
      <div className="overflow-x-auto">
        <PoolTable 
          pools={poolList} 
          onSelectPool={() => {}} 
          onDeletePool={handleDelete} 
          isDeleting={isDeleting} 
          deletingPools={deletingPools}
        />
      </div>
    </div>
  );
};

export default AllPools;
