import React from 'react';
import useClaimRewards from '@/hooks/useClaimRewards';
import { useAccount } from 'wagmi';
import useGetUserRewards from '@/hooks/useGetUserRewards';
import { formatCryptoNumber } from '@/utils/formatNumber';

interface ClaimRewardsFormProps {
  poolAddress: `0x${string}`;
}

const ClaimRewardsForm: React.FC<ClaimRewardsFormProps> = ({ poolAddress }) => {
  const { address } = useAccount();
  const {
    totalRewards,
    isLoading: isLoadingRewards,
    isError: isErrorRewards,
    error: errorRewards,
    refetch: refetchRewards,
  } = useGetUserRewards(poolAddress, address as `0x${string}`);

  const {
    handleClaimRewards,
    isLoading: isClaiming,
    isSuccess: isClaimSuccess,
    isError: isClaimError,
  } = useClaimRewards({ poolAddress, refetchRewards });

  const hasRewards = totalRewards !== '0';

  return (
    <div className="bg-gray-800 p-4 rounded-lg shadow-lg">
      {hasRewards ? (
        <>
          <p className="text-white">Total Rewards: {formatCryptoNumber(totalRewards)}</p>
          <form onSubmit={handleClaimRewards}>
            <button
              type="submit"
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
              disabled={isClaiming}
            >
              {isClaiming ? 'Claiming...' : 'Claim Rewards'}
            </button>
            {isClaimSuccess && <p className="mt-2 text-green-500">Rewards claimed successfully!</p>}
            {isClaimError && <p className="mt-2 text-red-500">Error claiming rewards</p>}
          </form>
        </>
      ) : (
        <p className="text-white">No rewards available</p>
      )}
      {isLoadingRewards && <p className="mt-2 text-yellow-500">Loading rewards...</p>}
      {isErrorRewards && <p className="mt-2 text-red-500">Error loading rewards: {errorRewards?.message}</p>}
    </div>
  );
};

export default ClaimRewardsForm;
