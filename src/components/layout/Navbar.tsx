"use client";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import ConnectWalletButton from "../ConnectWalletButton/ConnectWalletButton";
import { useTheme } from "next-themes";
import { Moon, Sun } from "lucide-react";
import { Button } from "@/components/ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { useAdminStatus } from "@/hooks/useAdminStatus";
import useGetAllAdmins from "@/hooks/useGetAllAdmin";
import { useUser } from "@/context/userContext";
import Register from "@/components/Register/Register";
import Login from "@/components/Connexion/Connexion";

export default function Navbar() {
  const {
    admins,
    isLoading: isLoadingAdmins,
    error: adminError,
  } = useGetAllAdmins();
  const isAdmin = useAdminStatus();
  const { setTheme } = useTheme();
  const { user } = useUser();
  const localAdmin = admins.filter(
    (admin: string) => admin === user?.walletAddress
  );
  const [isRegisterModalOpen, setIsRegisterModalOpen] = useState(false);
  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);

  const clearLocalstorage = () => {
    localStorage.clear();
    window.location.reload();
  };
  return (
    <div className="px-4 border backdrop-blur-3xl sticky top-0 z-10 py-4">
      <div className="flex justify-between items-center text-black dark:text-gray-200">
        <div className="flex space-x-6">
          {isAdmin && localAdmin.length !== 0 && (
            <Link href="/admindashboard">
              <Button variant="ghost">Admin Dashboard</Button>
            </Link>
          )}
          <Link href="/">
            <Button variant="ghost">Swap</Button>
          </Link>
          <Link href="/pools">
            <Button variant="ghost">Pools</Button>
          </Link>
          <Link href="/staking">
            <Button variant="ghost">Staking</Button>
          </Link>
          <Link href="/token_list">
            <Button variant="ghost">Tokens</Button>
          </Link>
        </div>
        <div className="flex space-x-6 items-center ml-auto">
          {user && (
            <Button variant="ghost" onClick={() => clearLocalstorage()}>
              Logout
            </Button>
          )}
          {!user ? (
            <>
              <Button
                variant="ghost"
                onClick={() => setIsRegisterModalOpen(true)}
              >
                Register
              </Button>
              <Register
                isOpen={isRegisterModalOpen}
                onClose={() => setIsRegisterModalOpen(false)}
              />
              <Button variant="ghost" onClick={() => setIsLoginModalOpen(true)}>
                Login
              </Button>
              <Login
                isOpen={isLoginModalOpen}
                onClose={() => setIsLoginModalOpen(false)}
              />
            </>
          ) : (
            <Button variant="secondary" disabled>
              {user?.firstName}
            </Button>
          )}

          <ConnectWalletButton />

          <DropdownMenu>
            <DropdownMenuTrigger asChild>
              <Button variant="ghost" size="icon">
                <Sun className="h-[1.2rem] w-[1.2rem] rotate-0 scale-100 transition-all dark:-rotate-90 dark:scale-0" />
                <Moon className="absolute h-[1.2rem] w-[1.2rem] rotate-90 scale-0 transition-all dark:rotate-0 dark:scale-100" />
                <span className="sr-only">Toggle theme</span>
              </Button>
            </DropdownMenuTrigger>
            <DropdownMenuContent align="end">
              <DropdownMenuItem onClick={() => setTheme("light")}>
                Light
              </DropdownMenuItem>
              <DropdownMenuItem onClick={() => setTheme("dark")}>
                Dark
              </DropdownMenuItem>
            </DropdownMenuContent>
          </DropdownMenu>
        </div>
      </div>
    </div>
  );
}
