"use client";

const Footer = () => {
  return (
    <footer className="border backdrop-blur-3xl fixed inset-x-0 bottom-0 text-center p-4">
      <p>ESGI - DEX © 2024</p>
    </footer>
  );
};

export default Footer;
