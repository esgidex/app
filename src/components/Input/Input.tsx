interface NumberInputProps {
  id: string;
  value: string; 
  onChange: (value: string) => void;
}

const Input = ({ id, value, onChange }: NumberInputProps) => {
  return (
    <div className="flex flex-col bg-dark-500 p-4 rounded-xl shadow-md">
      <input
        type="text"
        id={id}
        value={value}
        onChange={(e) => onChange(e.target.value)}
        className="bg-transparent text-white text-lg rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2"
        placeholder="0.0"
      />
      <span className="text-gray-400 text-sm mt-2">$0.00</span>
    </div>
  );
};

export default Input;
