import React, { useEffect, useState } from "react";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { useUsersApi } from "@/hooks/useUsersApi";
import ConnectWalletButton from "../ConnectWalletButton/ConnectWalletButton";
import { useAccount } from "wagmi";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Label } from "@/components/ui/label";

const Register = ({
  isOpen,
  onClose,
}: {
  isOpen: boolean;
  onClose: () => void;
}) => {
  const [isRegistering, setIsRegistering] = useState(false);
  const [firstName, setFirstName] = useState<string>("");
  const [lastName, setLastName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [walletAddress, setWalletAddress] = useState<string>("");
  const [role] = useState<string>("user");
  const [password, setPassword] = useState<string>("");
  const { createUser } = useUsersApi();
  const { address } = useAccount();

  useEffect(() => {
    setWalletAddress(address || "");
  }, [address]);

  return (
    <Dialog open={isOpen} onOpenChange={onClose}>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Register</DialogTitle>
          <DialogDescription>
            You also need to connect your wallet to sign up.
          </DialogDescription>
        </DialogHeader>
        <div className="grid gap-4 py-4">
          <form
            onSubmit={(e) => {
              e.preventDefault();
              setIsRegistering(true);
              createUser({
                firstName,
                lastName,
                email,
                walletAddress,
                role,
                password,
              }).finally(() => setIsRegistering(false));
            }}
            className="flex flex-col space-y-4"
          >
            <div className="grid grid-cols-4 items-center gap-4">
              <Label htmlFor="firstName" className="text-left">
                First Name
              </Label>
              <Input
                placeholder="John"
                id="firstName"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                className="col-span-3"
              />
            </div>
            <div className="grid grid-cols-4 items-center gap-4">
              <Label htmlFor="lastName" className="text-left">
                Last Name
              </Label>
              <Input
                placeholder="Doe"
                id="lastName"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                className="col-span-3"
              />
            </div>
            <div className="grid grid-cols-4 items-center gap-4">
              <Label htmlFor="email" className="text-left">
                Email
              </Label>
              <Input
                placeholder="JohnDoe@gmail.com"
                id="email"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className="col-span-3"
              />
            </div>
            <div className="grid grid-cols-4 items-center gap-4">
              <Label htmlFor="password" className="text-left">
                Password
              </Label>
              <Input
                placeholder="********"
                id="password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="col-span-3"
              />
            </div>
            <div className="flex gap-6 flex-row-reverse">
              <DialogFooter>
                <Button type="submit" disabled={isRegistering}>
                  Register
                </Button>
              </DialogFooter>
              <ConnectWalletButton />
            </div>
          </form>
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default Register;
