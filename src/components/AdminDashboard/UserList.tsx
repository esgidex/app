"use client";
import React from "react";
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Avatar, AvatarFallback } from "@/components/ui/avatar";
import {
  Pagination,
  PaginationContent,
  PaginationItem,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
} from "@/components/ui/pagination";
import { UserMinus, UserPlus } from "lucide-react";
import useAddAdmin from "@/hooks/useAddAdmin";
import useGetAllAdmins from "@/hooks/useGetAllAdmin";
import useGetAllBannedUsers from "@/hooks/useGetAllBannedUsers";
import useBanUser from "@/hooks/useBanUser";
import useUnbannedUser from "@/hooks/useUnbannedUser";
import useRemoveAdmin from "@/hooks/useRemoveAdmin";
import { useUsersApi, User } from "@/hooks/useUsersApi";

const formatAddress = (address: string) =>
  `${address.slice(0, 6)}...${address.slice(-4)}`;

const UserList = ({
  title,
  icon: Icon,
  users,
  page,
  setPage,
  totalPages,
  firstButtonLabel,
  buttonLabel,
}: any) => {
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const { createUser, deleteUser, updateUser } = useUsersApi();
  const [isCreateUserSuccess, setIsCreateUserSuccess] = React.useState<boolean>(false);
  const [isCreateUserFailure, setIsCreateUserFailure] = React.useState<boolean>(false);
  const [firstName, setFirstName] = React.useState<string>("");
  const [lastName, setLastName] = React.useState<string>("");
  const [email, setEmail] = React.useState<string>("");
  const [walletAddress, setWalletAddress] = React.useState<string>("");
  const [role, setRole] = React.useState<string>("");
  const [isDeletingUser, setIsDeletingUser] = React.useState<boolean>(false);
  const [isUpdatingUser, setIsUpdatingUser] = React.useState<boolean>(false);
  const [updatingFirstName, setUpdatingFirstName] = React.useState<string>("");
  const [updatingLastName, setUpdatingLastName] = React.useState<string>("");
  const [updatingEmail, setUpdatingEmail] = React.useState<string>("");
  const [updatingWalletAddress, setUpdatingWalletAddress] = React.useState<string>("");
  const [updatingRole, setUpdatingRole] = React.useState<string>("");

  const {
    adminAddress,
    setAdminAddress,
    handleCreateAdmin,
    isCreating,
    isSuccess: isAdminSuccess,
    isError: isAdminError,
    error: adminError,
  } = useAddAdmin();

  const {
    userAddress,
    setUserAddress,
    handleBanUser,
    isBanning,
    isSuccess: isBanSuccess,
    isError: isBanError,
    error: banError,
  } = useBanUser();

  const {
    handleUnbannedUser,
    isUnbanning,
    isSuccess: isUnbanSuccess,
    isError: isUnbanError,
    error: unbanError,
  } = useUnbannedUser();

  const {
    handleRemoveAdmin,
    isRemoving,
    isSuccess: isRemoveAdminSuccess,
    isError: isRemoveAdminError,
    error: removeAdminError,
  } = useRemoveAdmin();

  const {
    admins,
    isLoading: isLoadingAdmins,
    error: fetchAdminError,
  } = useGetAllAdmins();
  const {
    bannedUsers,
    isLoading: isLoadingBannedUsers,
    error: fetchBannedError,
  } = useGetAllBannedUsers();

  const isLoading =
    title === "Admin list" ? isLoadingAdmins : isLoadingBannedUsers;
  const fetchError =
    title === "Admin list" ? fetchAdminError : fetchBannedError;

  let list = users;

  if (title === "Admin list") {
    list = admins;
  } else if (title === "Banned users list") {
    list = bannedUsers;
  }

  const handleDeleteClick = (address: string) => {
    handleRemoveAdmin(address);
  };

  const handleUnbanClick = (address: string) => {
    handleUnbannedUser(address);
  };

  return (
    <Card x-chunk="dashboard-01-chunk-5" className="xl:col-span-2">
      <CardHeader>
        <CardTitle>{title}</CardTitle>
        {title === "Admin list" && (
          <div className="pt-2">
            <form className="sm:flex-initial" onSubmit={handleCreateAdmin}>
              <div className="relative flex flex-initial justify-between">
                <UserPlus className="absolute left-2.5 top-2.5 h-4 w-4 text-muted-foreground" />
                <Input
                  type="text"
                  placeholder="0x56789.....5678DDD"
                  value={adminAddress}
                  onChange={(e) => setAdminAddress(e.target.value)}
                  className="pl-8 w-[300px]"
                />
                <Button type="submit" disabled={isCreating}>
                  {isCreating ? "Adding..." : firstButtonLabel}
                </Button>
              </div>
            </form>
            {isAdminSuccess && (
              <p className="mt-2 text-green-500">Admin ajouté avec succès !</p>
            )}
            {isAdminError && (
              <p className="mt-2 text-red-500">Erreur : {adminError}</p>
            )}
          </div>
        )}
        {title === "Users" && (
          <div className="pt-2">
            <form className="sm:flex-initial" onSubmit={() => {
              createUser({
                firstName: firstName,
                lastName: lastName,
                email: email,
                walletAddress: walletAddress,
                role: role,
              })
                .then(() => {
                  setIsCreateUserSuccess(true);
                  setIsCreateUserFailure(false);
                }).catch(() => {
                  setIsCreateUserSuccess(false);
                  setIsCreateUserFailure(true);
                });
            }}>
              <div className="relative flex flex-col space-y-4">
                <div className="relative">
                  <UserPlus className="absolute left-2.5 top-2.5 h-4 w-4 text-muted-foreground" />
                  <Input
                    type="text"
                    placeholder="jeann"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    className="pl-8 w-[300px]"
                  />
                </div>
                <div className="relative">
                  <Input
                    type="text"
                    placeholder="dupont"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    className="pl-8 w-[300px]"
                  />
                </div>
                <div className="relative">
                  <Input
                    type="text"
                    placeholder="jean@gmail.com"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    className="pl-8 w-[300px]"
                  />
                </div>
                <div className="relative">
                  <Input
                    type="text"
                    placeholder="0x56789.....5678DDD"
                    value={walletAddress}
                    onChange={(e) => setWalletAddress(e.target.value)}
                    className="pl-8 w-[300px]"
                  />
                </div>
                <div className="relative">
                  <Input
                    type="text"
                    placeholder="user"
                    value={role}
                    onChange={(e) => setRole(e.target.value)}
                    className="pl-8 w-[300px]"
                  />
                </div>
                <div className="relative">
                  <Button variant="secondary" type="submit" disabled={isCreating}>
                    {isCreating ? "Adding..." : firstButtonLabel}
      
                  </Button>
                </div>
              </div>
            </form>
            {isCreateUserSuccess && (
              <p className="mt-2 text-green-500">User ajouté avec succès !</p>
            )}
            {isCreateUserFailure && (
              <p className="mt-2 text-red-500">Erreur</p>
            )}
          </div>
        )}
        {title === "Banned users list" && (
          <div className="pt-2">
            <form className="sm:flex-initial" onSubmit={handleBanUser}>
              <div className="relative flex flex-initial justify-between">
                <UserMinus className="absolute left-2.5 top-2.5 h-4 w-4 text-muted-foreground" />
                <Input
                  type="text"
                  placeholder="0x56789.....5678DDD"
                  value={userAddress}
                  onChange={(e) => setUserAddress(e.target.value)}
                  className="pl-8 w-[300px]"
                />
                <Button type="submit" disabled={isBanning}>
                  {isBanning ? "Banning..." : firstButtonLabel}
                </Button>
              </div>
            </form>
            {isBanSuccess && (
              <p className="mt-2 text-green-500">
                Utilisateur banni avec succès !
              </p>
            )}
            {isBanError && (
              <p className="mt-2 text-red-500">Erreur : {banError}</p>
            )}
          </div>
        )}
      </CardHeader>
      <CardContent className="grid gap-8">
        {list.map((user: string | User, index: number) => (
          <div key={index} className="flex items-center gap-4">
            {title === 'Users' && isModalOpen && (
              <div className="fixed inset-0 flex items-center justify-center z-50 bg-gray-800 bg-opacity-50">
                <div className="bg-white p-6 rounded shadow-lg w-[400px]">
                  <div className="flex justify-end">
                    <button onClick={() => setIsModalOpen(false)} className="text-gray-500 hover:text-gray-700">
                      &times;
                    </button>
                  </div>
                  <form className="sm:flex-initial" onSubmit={(e) => {
                    e.preventDefault();
                    user = user as User;
                    if (user.id === undefined) return;
                    setIsUpdatingUser(true);
                    updateUser(user.id, {
                      firstName: updatingFirstName,
                      lastName: updatingLastName,
                      email: updatingEmail,
                      walletAddress: updatingWalletAddress,
                      role: updatingRole,
                    })
                      .finally(() => setIsUpdatingUser(false));
                  }}>
                    <div className="relative flex flex-col space-y-4">
                      <div className="relative">
                        <UserPlus className="absolute left-2.5 top-2.5 h-4 w-4 text-muted-foreground" />
                        <Input
                          type="text"
                          placeholder="jeann"
                          value={updatingFirstName}
                          onChange={(e) => setUpdatingFirstName(e.target.value)}
                          className="pl-8 w-[300px] text-black"
                        />
                      </div>
                      <div className="relative">
                        <Input
                          type="text"
                          placeholder="dupont"
                          value={updatingLastName}
                          onChange={(e) => setUpdatingLastName(e.target.value)}
                          className="pl-8 w-[300px] text-black"
                        />
                      </div>
                      <div className="relative">
                        <Input
                          type="text"
                          placeholder="jean@gmail.com"
                          value={updatingEmail}
                          onChange={(e) => setUpdatingEmail(e.target.value)}
                          className="pl-8 w-[300px] text-black"
                        />
                      </div>
                      <div className="relative">
                        <Input
                          type="text"
                          placeholder="0x56789.....5678DDD"
                          value={updatingWalletAddress}
                          onChange={(e) => setUpdatingWalletAddress(e.target.value)}
                          className="pl-8 w-[300px] text-black"
                        />
                      </div>
                      <div className="relative">
                        <Input
                          type="text"
                          placeholder="user"
                          value={updatingRole}
                          onChange={(e) => setUpdatingRole(e.target.value)}
                          className="pl-8 w-[300px] text-black"
                        />
                      </div>
                      <div className="relative">
                        <Button variant="secondary" type="submit" disabled={isUpdatingUser}>
                          {isUpdatingUser ? "Updating..." : "Update User"}
                        </Button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            )}
            <Avatar className="hidden h-9 w-9 sm:flex">
              <AvatarFallback>{
              typeof user === 'string' ? user.slice(0, 2).toUpperCase() : (user as User).firstName.slice(0, 2).toUpperCase()}
              </AvatarFallback>
            </Avatar>
            <div className="grid gap-1">
              <p className="text-sm font-medium leading-none">{
              typeof user === 'string' ? user : (user as User).firstName + ' ' + (user as User).lastName}
              </p>
            </div>
            <div className="ml-auto font-medium">
              {title === "Admin list" && (
                <Button
                  variant="secondary"
                  onClick={() => handleDeleteClick(user as string)}
                  disabled={isRemoving}
                >
                  {isRemoving ? "Removing..." : buttonLabel}
                </Button>
              )}
              {title === "Banned users list" && (
                <Button
                  variant="secondary"
                  onClick={() => handleUnbanClick(user as string)}
                  disabled={isUnbanning}
                >
                  {isUnbanning ? "Unbanning..." : buttonLabel}
                </Button>
              )}
              {title === "Users" && (
                <>
                  <Button
                    variant="secondary"
                    onClick={() => {
                      setIsDeletingUser(true);
                      user = user as User;
                      if (user.id === undefined) return;
                      deleteUser(user.id)
                        .finally(() => setIsDeletingUser(false));
                    }}
                    disabled={isDeletingUser}
                  >
                    {isDeletingUser? "Unbanning..." : buttonLabel}
                  </Button>
                  <Button
                    variant="secondary"
                    onClick={() => {
                      setIsModalOpen(true);
                      setUpdatingFirstName((user as User).firstName);
                      setUpdatingLastName((user as User).lastName);
                      setUpdatingEmail((user as User).email);
                      setUpdatingWalletAddress((user as User).walletAddress);
                      setUpdatingRole((user as User).role);
                    }}
                    disabled={isUpdatingUser}
                  >
                    {isUpdatingUser ? "Updating..." : 'Update'}
                  </Button>
                </>
              )}
            </div>
          </div>
        ))}
      </CardContent>
      <CardFooter>
        <Pagination>
          <PaginationContent>
            <PaginationItem>
              <PaginationPrevious
                href="#"
                onClick={() => setPage((prev: any) => Math.max(prev - 1, 1))}
              />
            </PaginationItem>
            {Array.from({ length: totalPages }, (_, i) => (
              <PaginationItem key={i}>
                <PaginationLink
                  href="#"
                  onClick={() => setPage(i + 1)}
                  isActive={page === i + 1}
                >
                  {i + 1}
                </PaginationLink>
              </PaginationItem>
            ))}
            <PaginationItem>
              <PaginationNext
                href="#"
                onClick={() =>
                  setPage((prev: any) => Math.min(prev + 1, totalPages))
                }
              />
            </PaginationItem>
          </PaginationContent>
        </Pagination>
      </CardFooter>
    </Card>
  );
};

export default UserList;