"use client";

import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import { Button } from "@/components/ui/button";
import {
  Table,
  TableBody,
  TableCell,
  TableCaption,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";

const LiquidityPool = ({ pools }: any) => (
  <Card x-chunk="dashboard-01-chunk-5" className="col-span-full">
    <CardHeader>
      <CardTitle>Remove Liquidity Pool</CardTitle>
    </CardHeader>
    <CardContent>
      <Table>
        <TableCaption>A list of all pools created</TableCaption>
        <TableHeader>
          <TableRow>
            <TableHead className="w-[100px]">#</TableHead>
            <TableHead>Pools</TableHead>
            <TableHead>Volume</TableHead>
            <TableHead>Users</TableHead>
            <TableHead className="text-right">Actions</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody>
          {pools.map((pool: any) => (
            <TableRow key={pool.id}>
              <TableCell className="font-medium">{pool.id}</TableCell>
              <TableCell>{pool.pairs}</TableCell>
              <TableCell>{pool.volume}</TableCell>
              <TableCell>{pool.users}</TableCell>
              <TableCell className="text-right">
                <Button >Delete</Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </CardContent>
  </Card>
);

export default LiquidityPool;
