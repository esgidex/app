"use client";

import * as React from "react";
import { Area, AreaChart, CartesianGrid, XAxis } from "recharts";

import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import {
  ChartConfig,
  ChartContainer,
  ChartLegend,
  ChartLegendContent,
  ChartTooltip,
  ChartTooltipContent,
} from "@/components/ui/chart";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";

const chartData = [
  { date: "2024-04-01", Staking: 222, Liquidity_Pools: 150 },
  { date: "2024-04-02", Staking: 97, Liquidity_Pools: 180 },
  { date: "2024-04-03", Staking: 167, Liquidity_Pools: 120 },
  { date: "2024-04-04", Staking: 242, Liquidity_Pools: 260 },
  { date: "2024-04-05", Staking: 373, Liquidity_Pools: 290 },
  { date: "2024-04-06", Staking: 301, Liquidity_Pools: 340 },
  { date: "2024-04-07", Staking: 245, Liquidity_Pools: 180 },
  { date: "2024-04-08", Staking: 409, Liquidity_Pools: 320 },
  { date: "2024-04-09", Staking: 59, Liquidity_Pools: 110 },
  { date: "2024-04-10", Staking: 261, Liquidity_Pools: 190 },
  { date: "2024-04-11", Staking: 327, Liquidity_Pools: 350 },
  { date: "2024-04-12", Staking: 292, Liquidity_Pools: 210 },
  { date: "2024-04-13", Staking: 342, Liquidity_Pools: 380 },
  { date: "2024-04-14", Staking: 137, Liquidity_Pools: 220 },
  { date: "2024-04-15", Staking: 120, Liquidity_Pools: 170 },
  { date: "2024-04-16", Staking: 138, Liquidity_Pools: 190 },
  { date: "2024-04-17", Staking: 446, Liquidity_Pools: 360 },
  { date: "2024-04-18", Staking: 364, Liquidity_Pools: 410 },
  { date: "2024-04-19", Staking: 243, Liquidity_Pools: 180 },
  { date: "2024-04-20", Staking: 89, Liquidity_Pools: 150 },
  { date: "2024-04-21", Staking: 137, Liquidity_Pools: 200 },
  { date: "2024-04-22", Staking: 224, Liquidity_Pools: 170 },
  { date: "2024-04-23", Staking: 138, Liquidity_Pools: 230 },
  { date: "2024-04-24", Staking: 387, Liquidity_Pools: 290 },
  { date: "2024-04-25", Staking: 215, Liquidity_Pools: 250 },
  { date: "2024-04-26", Staking: 75, Liquidity_Pools: 130 },
  { date: "2024-04-27", Staking: 383, Liquidity_Pools: 420 },
  { date: "2024-04-28", Staking: 122, Liquidity_Pools: 180 },
  { date: "2024-04-29", Staking: 315, Liquidity_Pools: 240 },
  { date: "2024-04-30", Staking: 454, Liquidity_Pools: 380 },
  { date: "2024-05-01", Staking: 165, Liquidity_Pools: 220 },
  { date: "2024-05-02", Staking: 293, Liquidity_Pools: 310 },
  { date: "2024-05-03", Staking: 247, Liquidity_Pools: 190 },
  { date: "2024-05-04", Staking: 385, Liquidity_Pools: 420 },
  { date: "2024-05-05", Staking: 481, Liquidity_Pools: 390 },
  { date: "2024-05-06", Staking: 498, Liquidity_Pools: 520 },
  { date: "2024-05-07", Staking: 388, Liquidity_Pools: 300 },
  { date: "2024-05-08", Staking: 149, Liquidity_Pools: 210 },
  { date: "2024-05-09", Staking: 227, Liquidity_Pools: 180 },
  { date: "2024-05-10", Staking: 293, Liquidity_Pools: 330 },
  { date: "2024-05-11", Staking: 335, Liquidity_Pools: 270 },
  { date: "2024-05-12", Staking: 197, Liquidity_Pools: 240 },
  { date: "2024-05-13", Staking: 197, Liquidity_Pools: 160 },
  { date: "2024-05-14", Staking: 448, Liquidity_Pools: 490 },
  { date: "2024-05-15", Staking: 473, Liquidity_Pools: 380 },
  { date: "2024-05-16", Staking: 338, Liquidity_Pools: 400 },
  { date: "2024-05-17", Staking: 499, Liquidity_Pools: 420 },
  { date: "2024-05-18", Staking: 315, Liquidity_Pools: 350 },
  { date: "2024-05-19", Staking: 235, Liquidity_Pools: 180 },
  { date: "2024-05-20", Staking: 177, Liquidity_Pools: 230 },
  { date: "2024-05-21", Staking: 82, Liquidity_Pools: 140 },
  { date: "2024-05-22", Staking: 81, Liquidity_Pools: 120 },
  { date: "2024-05-23", Staking: 252, Liquidity_Pools: 290 },
  { date: "2024-05-24", Staking: 294, Liquidity_Pools: 220 },
  { date: "2024-05-25", Staking: 201, Liquidity_Pools: 250 },
  { date: "2024-05-26", Staking: 213, Liquidity_Pools: 170 },
  { date: "2024-05-27", Staking: 420, Liquidity_Pools: 460 },
  { date: "2024-05-28", Staking: 233, Liquidity_Pools: 190 },
  { date: "2024-05-29", Staking: 78, Liquidity_Pools: 130 },
  { date: "2024-05-30", Staking: 340, Liquidity_Pools: 280 },
  { date: "2024-05-31", Staking: 178, Liquidity_Pools: 230 },
  { date: "2024-06-01", Staking: 178, Liquidity_Pools: 200 },
  { date: "2024-06-02", Staking: 470, Liquidity_Pools: 410 },
  { date: "2024-06-03", Staking: 103, Liquidity_Pools: 160 },
  { date: "2024-06-04", Staking: 439, Liquidity_Pools: 380 },
  { date: "2024-06-05", Staking: 88, Liquidity_Pools: 140 },
  { date: "2024-06-06", Staking: 294, Liquidity_Pools: 250 },
  { date: "2024-06-07", Staking: 323, Liquidity_Pools: 370 },
  { date: "2024-06-08", Staking: 385, Liquidity_Pools: 320 },
  { date: "2024-06-09", Staking: 438, Liquidity_Pools: 480 },
  { date: "2024-06-10", Staking: 155, Liquidity_Pools: 200 },
  { date: "2024-06-11", Staking: 92, Liquidity_Pools: 150 },
  { date: "2024-06-12", Staking: 492, Liquidity_Pools: 420 },
  { date: "2024-06-13", Staking: 81, Liquidity_Pools: 130 },
  { date: "2024-06-14", Staking: 426, Liquidity_Pools: 380 },
  { date: "2024-06-15", Staking: 307, Liquidity_Pools: 350 },
  { date: "2024-06-16", Staking: 371, Liquidity_Pools: 310 },
  { date: "2024-06-17", Staking: 475, Liquidity_Pools: 520 },
  { date: "2024-06-18", Staking: 107, Liquidity_Pools: 170 },
  { date: "2024-06-19", Staking: 341, Liquidity_Pools: 290 },
  { date: "2024-06-20", Staking: 408, Liquidity_Pools: 450 },
  { date: "2024-06-21", Staking: 169, Liquidity_Pools: 210 },
  { date: "2024-06-22", Staking: 317, Liquidity_Pools: 270 },
  { date: "2024-06-23", Staking: 480, Liquidity_Pools: 530 },
  { date: "2024-06-24", Staking: 132, Liquidity_Pools: 180 },
  { date: "2024-06-25", Staking: 141, Liquidity_Pools: 190 },
  { date: "2024-06-26", Staking: 434, Liquidity_Pools: 380 },
  { date: "2024-06-27", Staking: 448, Liquidity_Pools: 490 },
  { date: "2024-06-28", Staking: 149, Liquidity_Pools: 200 },
  { date: "2024-06-29", Staking: 103, Liquidity_Pools: 160 },
  { date: "2024-06-30", Staking: 46, Liquidity_Pools: 100 },
  { date: "2024-07-01", Staking: 346, Liquidity_Pools: 0 },
  { date: "2024-07-02", Staking: 146, Liquidity_Pools: 40 },
  { date: "2024-07-03", Staking: 246, Liquidity_Pools: 60 },
  { date: "2024-07-04", Staking: 400, Liquidity_Pools: 100 },
  { date: "2024-07-05", Staking: 200, Liquidity_Pools: 300 },
  { date: "2024-07-06", Staking: 111, Liquidity_Pools: 450 },
  { date: "2024-07-07", Staking: 423, Liquidity_Pools: 460 },
  { date: "2024-07-08", Staking: 600, Liquidity_Pools: 900 },
  { date: "2024-07-09", Staking: 431, Liquidity_Pools: 100 },
  { date: "2024-07-10", Staking: 446, Liquidity_Pools: 60 },
  { date: "2024-07-11", Staking: 946, Liquidity_Pools: 100 },
  { date: "2024-07-12", Staking: 146, Liquidity_Pools: 300 },
  { date: "2024-07-13", Staking: 246, Liquidity_Pools: 130 },
  { date: "2024-07-14", Staking: 146, Liquidity_Pools: 700 },
  { date: "2024-07-15", Staking: 46, Liquidity_Pools: 800 },
  { date: "2024-07-16", Staking: 346, Liquidity_Pools: 400 },
  { date: "2024-07-17", Staking: 146, Liquidity_Pools: 130 },
  { date: "2024-07-18", Staking: 446, Liquidity_Pools: 400 },
  { date: "2024-07-19", Staking: 446, Liquidity_Pools: 400 },
  { date: "2024-07-20", Staking: 446, Liquidity_Pools: 130 },
];

const chartConfig = {
  visitors: {
    label: "Visitors",
  },
  Staking: {
    label: "Staking",
    color: "hsl(var(--chart-1))",
  },
  Liquidity_Pools: {
    label: "Liquidity Pools",
    color: "hsl(var(--chart-2))",
  },
} satisfies ChartConfig;

const MoreStats = ({ open, setOpen, value, setValue, addresses }: any) => {
  const [timeRange, setTimeRange] = React.useState("90d");

  const filteredData = chartData.filter((item) => {
    const date = new Date(item.date);
    const now = new Date();
    let daysToSubtract = 90;
    if (timeRange === "30d") {
      daysToSubtract = 30;
    } else if (timeRange === "7d") {
      daysToSubtract = 7;
    }
    now.setDate(now.getDate() - daysToSubtract);
    return date >= now;
  });

  return (
    <Card x-chunk="dashboard-01-chunk-5" className="col-span-full">
      <CardHeader className="flex items-center gap-2 space-y-0 border-b py-5 sm:flex-row">
        <div className="grid flex-1 gap-1 text-center sm:text-left">
          <CardTitle>More stats</CardTitle>
          <CardDescription>
            Overview of the staking and liquidity pools
          </CardDescription>
        </div>
        <Select value={timeRange} onValueChange={setTimeRange}>
          <SelectTrigger
            className="w-[160px] rounded-lg sm:ml-auto"
            aria-label="Select a value"
          >
            <SelectValue placeholder="Last 3 months" />
          </SelectTrigger>
          <SelectContent className="rounded-xl">
            <SelectItem value="90d" className="rounded-lg">
              Last 3 months
            </SelectItem>
            <SelectItem value="30d" className="rounded-lg">
              Last 30 days
            </SelectItem>
            <SelectItem value="7d" className="rounded-lg">
              Last 7 days
            </SelectItem>
          </SelectContent>
        </Select>
      </CardHeader>
      <CardContent className="px-2 pt-4 sm:px-6 sm:pt-6">
        <ChartContainer
          config={chartConfig}
          className="aspect-auto h-[250px] w-full"
        >
          <AreaChart data={filteredData}>
            <defs>
              <linearGradient id="fillStaking" x1="0" y1="0" x2="0" y2="1">
                <stop
                  offset="5%"
                  stopColor="var(--color-Staking)"
                  stopOpacity={0.8}
                />
                <stop
                  offset="95%"
                  stopColor="var(--color-Staking)"
                  stopOpacity={0.1}
                />
              </linearGradient>
              <linearGradient id="fillLiquidity_Pools" x1="0" y1="0" x2="0" y2="1">
                <stop
                  offset="5%"
                  stopColor="var(--color-Liquidity_Pools)"
                  stopOpacity={0.8}
                />
                <stop
                  offset="95%"
                  stopColor="var(--color-Liquidity_Pools)"
                  stopOpacity={0.1}
                />
              </linearGradient>
            </defs>
            <CartesianGrid vertical={false} />
            <XAxis
              dataKey="date"
              tickLine={false}
              axisLine={false}
              tickMargin={8}
              minTickGap={32}
              tickFormatter={(value) => {
                const date = new Date(value);
                return date.toLocaleDateString("en-US", {
                  month: "short",
                  day: "numeric",
                });
              }}
            />
            <ChartTooltip
              cursor={false}
              content={
                <ChartTooltipContent
                  labelFormatter={(value) => {
                    return new Date(value).toLocaleDateString("en-US", {
                      month: "short",
                      day: "numeric",
                    });
                  }}
                  indicator="dot"
                />
              }
            />
            <Area
              dataKey="Liquidity_Pools"
              type="natural"
              fill="url(#fillLiquidity_Pools)"
              stroke="var(--color-Liquidity_Pools)"
              stackId="a"
            />
            <Area
              dataKey="Staking"
              type="natural"
              fill="url(#fillStaking)"
              stroke="var(--color-Staking)"
              stackId="a"
            />
            <ChartLegend content={<ChartLegendContent />} />
          </AreaChart>
        </ChartContainer>
      </CardContent>
    </Card>
  );
};

export default MoreStats;
