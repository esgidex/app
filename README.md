# dex-esgi

ADDRESS CONTRACT : 

Mock ETH (mETH) : 0x099b7E1bE4Bf8fbf4a9a8b9E0E0511cAc4Ead615

mock matic (mMATIC) : 0xADEaBfFD27CAE5dFc0e24D6A1354b2641A86615F

contrat liquidityToken : 0x7ba03e409955d22dea5fa7a2b6e38f20dec01e4e

contrat PoolContract : 0xBA3e04517ccabFAe88E9eFB9B9020196Fca56c51

wallet connect projectid : b62b8ce36c1fa3ea4a60323c740b76d0

contrat poolFactory : 0x0bC171365514c69EC3C3A26048882389a5c2c302

pool mETH & mMatic : 0x2f28B25347884B232Af6b82Ba3b473606CcaF29b


=======================================================================  TESTS ========================================================================

autre link token : 0x779877A7B0D9E8603169DdbD7836e478b4624789
et dai : 0xFF34B3d4Aee8ddCd6F9AFFFB6Fe49bD371b8a357

price feed sépolia : 

- DAI / USD : 0x14866185B1962B63C3Ea9E03Bc1da838bab34C19
- LINK / USD : 0xc59E3633BAAC79493d908e63626716e204A45EdF
- ETH / USD : 0x694AA1769357215DE4FAC081bf1f309aDC325306



















TEST TOKENS address 

DAI-TestnetMintableERC20-Aave  │ '0xFF34B3d4Aee8ddCd6F9AFFFB6Fe49bD371b8a357' │
│ LINK-TestnetMintableERC20-Aave │ '0xf8Fb3713D459D7C1018BD0A49D19b4C44290EBE5' │
│ USDC-TestnetMintableERC20-Aave │ '0x94a9D9AC8a22534E3FaCa9F4e7F2E2cf85d5E4C8' │
│ WBTC-TestnetMintableERC20-Aave │ '0x29f2D40B0605204364af54EC677bD022dA425d03' │
│ WETH-TestnetMintableERC20-Aave │ '0xC558DBdd856501FCd9aaF1E62eae57A9F0629a3c' │
│ USDT-TestnetMintableERC20-Aave │ '0xaA8E23Fb1079EA71e0a56F48a2aA51851D8433D0' │
│ AAVE-TestnetMintableERC20-Aave │ '0x88541670E55cC00bEEFD87eB59EDd1b7C511AC9a' │
│ EURS-TestnetMintableERC20-Aave │ '0x6d906e526a4e2Ca02097BA9d0caA3c382F52278E' 

TESTS address 

poolFactory : 0x4e13c4b578fE2Cd63731420E34B373c3A5BC80A0

CA pricefeeds sur chainlink : 

- LINK / ETH : 0xDC530D9457755926550b59e8ECcdaE7624181557
- LINK / USD : 0x2c1d072e956AFFC0D435Cb7AC38EF18d24d9127c
- AAVE / ETH : 0x6Df09E975c830ECae5bd4eD9d90f3A95a4f88012
- AAVE / USD : 0x547a514d5e3769680Ce22B2361c10Ea13619e8a9
- DAI / ETH : 0x773616E4d11A78F511299002da57A0a94577F1f4
- DAI / USD : 0xAed0c38402a5d19df6E4c03F4E2DceD6e29c1ee9
- USDC / ETH : 0x986b5E1e1755e3C2440e960477f25201B0a8bbD4
- USDC / USD : 0x8fFfFfd4AfB6115b954Bd326cbe7B4BA576818f6
- USDT /  ETH :  0xEe9F2375b4bdF6387aa8265dD4FB8F16512A1d46
- USDT / USD : 0x3E7d1eAB13ad0104d2750B8863b489D65364e32D

premiere pool -> link/aave avec pricefeed chainlink: 0x4B26eC873D80a5c74Db722Aa30a6b09943F5Dab8

pool test : 0x4bFa28B409fE19e404eA501a4254E7454F5B4B90


STEP BY STEP SWAP

+---------------------------------+
|          Interface Utilisateur  |
+---------------------------------+
| 1. Saisir les adresses          |
|    - TokenIn                    |
|    - TokenOut                   |
| 2. Saisir le montant à échanger |
|    - amountIn                   |
| 3. Cliquer sur "Swap"           |
+---------------------------------+
                |
                v
+---------------------------------+
|            Frontend             |
+---------------------------------+
| 1. Vérifier les entrées         |
| 2. Appeler la fonction          |
|    "estimateSwap" pour          |
|    calculer les montants et     |
|    les frais                    |
| 3. Afficher les résultats       |
|    - amountOut                  |
|    - fee                        |
| 4. Demander confirmation à      |
|    l'utilisateur                |
| 5. Si confirmé, appeler la      |
|    fonction "approve" sur       |
|    le contrat ERC20             |
| 6. Appeler la fonction "swap"   |
|    sur ESGIDex                  |
+---------------------------------+
                |
                v
+---------------------------------+
|        Contrat ESGIDex          |
+---------------------------------+
| 1. Validate                     |
|    - TokenIn ≠ TokenOut         |
|    - AmountIn > 0               |
|    - Pool Exists                |
|    - Sufficient Reserves        |
| 2. Calculate                    |
|    - Fee                        |
|    - AmountInAfterFee           |
|    - AmountOut                  |
| 3. Transfer                     |
|    - Transfer TokenIn from      |
|      user to pool               |
|    - Transfer TokenOut from     |
|      pool to user               |
|    - Distribute fees            |
| 4. Emit Events                  |
|    - TokensSwapped              |
+---------------------------------+
                |
                v
+---------------------------------+
|        Pool de Liquidité        |
+---------------------------------+
| 1. Mise à jour des réserves     |
| 2. Répartition des frais        |
+---------------------------------+


Étapes Utilisateur pour Swap
- Accès à la page de swap
- Connexion du compte de l'utilisateur
- Sélection des tokens à échanger
- Saisie du montant à échanger
- Estimation du montant de tokens reçus
- Vérification et approbation de l'allocation
- Exécution du swap